const gulp = require('gulp'),
  $ = require('gulp-load-plugins')(),
  open = require('open'),
  rename = require('gulp-rename'),
  htmlImport = require('gulp-html-import'),
  htmlmin = require('gulp-htmlmin'),
  port = 9216,
  index ="index",
  babel = require("gulp-babel"),
 connect = require('gulp-connect'),
 proxy = require('http-proxy-middleware'),
  // rjs = require('gulp-requirejs'),
  // optimize = require('gulp-requirejs-optimize'),
  options = {
    removeComments: true,//清除HTML注释
    collapseWhitespace: true,//压缩HTML
    collapseBooleanAttributes: true,//省略布尔属性的值 <input checked="true"/> ==> <input />
    removeEmptyAttributes: true,//删除所有空格作属性值 <input id="" /> ==> <input />
    removeScriptTypeAttributes: true,//删除<script>的type="text/javascript"
    removeStyleLinkTypeAttributes: true,//删除<style>和<link>的type="text/css"
    minifyJS: true,//压缩页面JS
    minifyCSS: true//压缩页面CSS
  },
app = {
  srcPath: 'src/',
  devPath: 'build/',
  prdPath: 'dist/'
};


gulp.task('lib', function () {
  gulp.src('bower_components/**/*')
    .pipe(gulp.dest(app.devPath + 'js/vendor'))
    .pipe(gulp.dest(app.prdPath + 'js/vendor'))
    .pipe($.connect.reload());
  gulp.src(app.srcPath + 'script/lib/**/*.js')
    .pipe(gulp.dest(app.devPath + 'js/lib/'))
    .pipe(gulp.dest(app.prdPath + 'js/lib/'))
    .pipe($.connect.reload())
});

gulp.task('html', function () {
  gulp.src(app.srcPath + 'html/*.html')
    .pipe(htmlImport(app.srcPath + 'html/components/'))
    .pipe(gulp.dest(app.devPath))
    .pipe(htmlmin(options))
    .pipe(gulp.dest(app.prdPath))
    .pipe($.connect.reload())
});

gulp.task('json', function () {
  gulp.src(app.srcPath + 'data/**/*')
    .pipe(gulp.dest(app.devPath + 'data'))
    .pipe(gulp.dest(app.prdPath + 'data'))
    .pipe($.connect.reload())
});

gulp.task('less', function () {
  gulp.src(app.srcPath + 'style/index.less')
    .pipe($.plumber())
    .pipe($.less())
    .pipe(gulp.dest(app.devPath + 'css'))
    .pipe($.cssmin())
    .pipe(gulp.dest(app.prdPath + 'css'))
    .pipe($.connect.reload())
});

gulp.task('css', function () {
  gulp.src(app.srcPath + 'style/*.css')
    .pipe($.plumber())
    //.pipe($.less())
    .pipe(gulp.dest(app.devPath + 'css'))
    .pipe($.cssmin())
    .pipe(gulp.dest(app.prdPath + 'css'))
    .pipe($.connect.reload())
});

gulp.task('js', function () {
  gulp.src(app.srcPath + 'script/app/**/*.js')
    .pipe($.plumber())
    .pipe(babel())
    //.pipe($.concat('main.js'))
    .pipe(gulp.dest(app.devPath + 'js'))
    .pipe($.uglify())
    //.pipe(rename("index.min.js"))
    .pipe(gulp.dest(app.prdPath + 'js'))
    .pipe($.connect.reload())
});

gulp.task('tpl', function () {
  gulp.src(app.srcPath + 'script/tpl/**/*.hbs')
    .pipe(gulp.dest(app.devPath + 'js/tpl/'))
    .pipe(gulp.dest(app.prdPath + 'js/tpl/'))
    .pipe($.connect.reload())
});

gulp.task('images', function () {
  gulp.src(app.srcPath + 'images/**/*')
    .pipe(gulp.dest(app.devPath + 'images'))
    .pipe($.imagemin())
    .pipe(gulp.dest(app.prdPath + 'images'))
    .pipe($.connect.reload())
});

gulp.task('build', ['lib', 'tpl', 'html', 'json', 'less', 'css', 'js', 'images']);

gulp.task('clean', function () {
  gulp.src([app.devPath, app.prdPath])
    .pipe($.clean());
});

gulp.task('server', ['build'], function () {
  $.connect.server({
    root: [app.devPath],
    livereload: true,
    port: port,
    middleware: function (connect, opt) {
      return [
        proxy('/api', {
          target: 'http://114.116.154.42:8099',//代理的目标地址
          changeOrigin:true,
          pathRewrite:{//路径重写规则
            '^/api':''
          }
        }),
        proxy('/test', {
          target: 'http://192.168.100.208:8088',//代理的目标地址
          changeOrigin:true,
          pathRewrite:{//路径重写规则
            '^/test':''
          }
        }),
        proxy('/xkTest', {
          target: 'http://192.168.100.85:8099',//代理的目标地址
          changeOrigin:true,
          pathRewrite:{//路径重写规则
            '^/xkTest':''
          }
        }),
        proxy('/plat', {
          target: 'http://59.173.8.185:8080',//代理的目标地址
          changeOrigin:true,
          pathRewrite:{//路径重写规则
            '^/plat':''
          }
        })
      ]
    }
  });

  open(`http://localhost:${port}/${index}.html`);

  gulp.watch('bower_components/**/*.js', ['lib']);
  gulp.watch(app.srcPath + 'script/**/*.js', ['lib']);
  gulp.watch(app.srcPath + 'script/**/*.hbs', ['tpl']);
  gulp.watch(app.srcPath + '**/*.html', ['html']);
  gulp.watch(app.srcPath + 'data/**/*.json', ['json']);
  gulp.watch(app.srcPath + 'style/**/*.less', ['less']);
  gulp.watch(app.srcPath + 'script/**/*.css', ['css']);
  gulp.watch(app.srcPath + 'script/**/*.js', ['js']);
  gulp.watch(app.srcPath + 'images/**/*', ['images']);
});


gulp.task('default', ['server']);
