"use strict";
require([
    './dev/layer',
    './dev/mockObj',
    './dev/commonHandle',
    './dev/tool',
    './dev/declare'
], function (layFrame, mock, commonHandle, tool, declare) {

    layui.use(['layer', 'form', 'element', 'table'],  () => {
        const  $ = layui.jquery,
          layer = layui.layer,
          element = layui.element,
          table = layui.table,
          form = layui.form;

        //mock.setMock(declare)

      // commonHandle.getDateList($, table, "/data/index","tabConf")

      commonHandle.getDateList($, table, "/enterprise/selectDeclareInfo","tabConf")

      commonHandle.toolbarPrice($, form, table, 'toolbar(test)', "declarations.html")

      commonHandle.setUserName($, "#userName");

      commonHandle.exitHandle($, "exitBtn", ["id", "userName", "enterpriseId"], "login.html")

    });


});
