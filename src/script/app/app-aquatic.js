"use strict";
require([
    './dev/tool',
    'text!tpl/queryInfo.hbs',
    'text!tpl/addAqu.hbs',
    'text!tpl/editAqu.hbs',
    './conf/urlObj',
    './dev/handleChart',
    'mock'
], function (tool, queryInfo, addAqu, editAqu, urlObj, handleChart, Mock) {

    layui.use(['layer','table', 'element', 'form', 'laydate'],  () => {
      const  $ = layui.jquery,
        table = layui.table,
        element = layui.element,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;


      // 单行操作（编辑/删除）
      handleChart.handeTool($, form, table,'tool(test)', '真的删除行么', 'exampleEdit', "/xcj/aproduct/findAproductlList", "tabAproduct", "/xcj/aproduct/changeFlag", "getAproductList", "aquatic")
      //工具行操作（审核提交/退回修改/批量删除/新增/复制新增）
      handleChart.toolbarPrice($, form, table, 'toolbar(test)', "没有选中指定的信息", 'exampleEdit', urlObj, "/xcj/aproduct/findAproductlList", "tabAproduct", ["/xcj/aproduct/changeState", "/xcj/aproduct/changeFlag", "/xcj/aproduct/addAproduct"], "getAproductList", "aquatic");
      //遍历获取品种及表单默认数据。
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findAquaticList", "/xcj/breed/findbazaarList"], "demo1", "queryBox", ["选择品种", "选择市场"], queryInfo, "queryDate")

      //获取后台默认数据，构建新增表单模板
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findAquaticList", "/xcj/breed/findbazaarList"], "demo4", "std", ["选择品种", "选择市场"], addAqu, "")
      //获取后台默认数据，构建编辑表单模板
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findAquaticList", "/xcj/breed/findbazaarList"], "demo5", "etd", ["选择品种", "选择市场"], editAqu, "")
      //查询表单(信息录入)
      handleChart.getAproductList($, table, urlObj, "/xcj/aproduct/findAproductlList", "tabAproduct");
      //退出登录
      tool.exitHandle($, "exitBtn", ["id", "bazaarIds", "token", "username"], "login.html");
      //根据表单条件查询数据（信息录入/汇总统计）
      handleChart.submitTip($, table, form, "demo1", "/xcj/aproduct/findAproductlList", handleChart['getAproductList'], "tabAproduct", "aquatic")

      //新增一条数据
      handleChart.submitAddPrice($, table, form, "demo4", handleChart['putAddInfo'], "/xcj/aproduct/addAproduct", "tabAproduct", "/xcj/aproduct/findAproductlList", "getAproductList", "aquatic")
      //编辑一条数据
      handleChart.submitAddPrice($, table, form, "demo5", handleChart['putEditInfo'], "/xcj/aproduct/updateAproduct", "tabAproduct", "/xcj/aproduct/findAproductlList",  "getAproductList", "aquatic")
      //全选或常用
      handleChart.checkSwitch($, form, laydate, "checkbox(c_one)", 'switch(switchTest)', "#c_all", ".checkSelt", 'checkbox', "checked", queryInfo, "getVariety", "demo1", ["/xcj/breed/findAquaticList", "/xcj/breed/findbazaarList"], "queryBox", ["选择品种", "选择市场"], "queryDate")
    });


});
