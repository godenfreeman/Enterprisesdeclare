"use strict";
require([
    //'polyfill',
    './dev/layer',
    './dev/http-lay',
    './dev/tool',
    'text!tpl/queryInfo.hbs',
    'text!tpl/addOil.hbs',
    'text!tpl/editOil.hbs',
    './conf/urlObj',
    './dev/handleChart'
], function ( layFrame, http, tool, queryInfo, addOil, editOil, urlObj, handleChart) {

    layui.use(['layer','table', 'element', 'form', 'laydate'],  () => {
      const  $ = layui.jquery,
        table = layui.table,
        element = layui.element,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;


      // 单行操作（编辑/删除）
      handleChart.handeTool($, form, table,'tool(test)', '真的删除行么', 'exampleEdit', "/xcj/grainOil/findGrainOilList", "tabOil", "/xcj/grainOil/changeFlag", "getGrainOilList", "grainOil")
      //工具行操作（审核提交/退回修改/批量删除/新增/复制新增）
      handleChart.toolbarPrice($, form, table, 'toolbar(test)', "没有选中指定的信息", 'exampleEdit', urlObj, "/xcj/grainOil/findGrainOilList", "tabOil", ["/xcj/grainOil/changeState", "/xcj/grainOil/changeFlag", "/xcj/grainOil/addGrainOil"], "getGrainOilList", "grainOil");
      //遍历获取品种及表单默认数据。
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findGrainOilList", "/xcj/breed/findbazaarList"], "demo1", "queryBox", ["选择品种", "选择市场"], queryInfo, "queryDate")
      //handleChart.getVariety($, form, laydate, ["/xcj/breed/findGrainOilList", "/xcj/breed/findbazaarList"], "demo2", "censusBox", ["选择品种", "选择市场"], queryInfo, "censusDate")
      //获取后台默认数据，构建新增表单模板
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findGrainOilList", "/xcj/breed/findbazaarList"], "demo4", "std", ["选择品种", "选择市场"], addOil, "")
      //获取后台默认数据，构建编辑表单模板
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findGrainOilList", "/xcj/breed/findbazaarList"], "demo5", "etd", ["选择品种", "选择市场"], editOil, "")

      //查询表单(信息录入)
      handleChart.getGrainOilList($, table, urlObj, "/xcj/grainOil/findGrainOilList", "tabOil");
      //查询表单(汇总统计)editOil
      //handleChart.getStatistisData($, table, urlObj, "/xcj/vegetables/findByStatistisData");
      //退出登录
      tool.exitHandle($, "exitBtn", ["id", "bazaarIds", "token", "username"], "login.html")
      //根据表单条件查询数据（信息录入/汇总统计）
      handleChart.submitTip($, table, form, "demo1", "/xcj/grainOil/findGrainOilList", handleChart['getGrainOilList'], "tabOil", "grainOil")
      //handleChart.submitTip($, table, form, "demo2", "/xcj/vegetables/findByStatistisData", handleChart['getStatistisData'])
      //新增一条数据
      handleChart.submitAddPrice($, table, form, "demo4", handleChart['putAddInfo'], "/xcj/grainOil/addGrainOil", "tabOil", "/xcj/grainOil/findGrainOilList", "getGrainOilList", "grainOil")
      //编辑一条数据
      handleChart.submitAddPrice($, table, form, "demo5", handleChart['putEditInfo'], "/xcj/grainOil/updateGrainOilById", "tabOil", "/xcj/grainOil/findGrainOilList",  "getGrainOilList", "grainOil")
      //全选或常用
      handleChart.checkSwitch($, form, laydate, "checkbox(c_one)", 'switch(switchTest)', "#c_all", ".checkSelt", 'checkbox', "checked", queryInfo, "getVariety", "demo1", ["/xcj/breed/findGrainOilList", "/xcj/breed/findbazaarList"], "queryBox", ["选择品种", "选择市场"], "queryDate")
    });


});
