"use strict";
require([
    './dev/tool',
    'text!tpl/queryliveStock.hbs',
    'text!tpl/addStock.hbs',
    'text!tpl/editStock.hbs',
    './conf/urlObj',
    './dev/handleChart',
    'mock'
], function (tool, queryliveStock, addStock, editStock, urlObj, handleChart, Mock) {

    layui.use(['layer','table', 'element', 'form', 'laydate'],  () => {
      const  $ = layui.jquery,
        table = layui.table,
        element = layui.element,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;


      // 单行操作（编辑/删除）
      handleChart.handeTool($, form, table,'tool(test)', '真的删除行么', 'exampleEdit', "/xcj/livestock/findLivestockList", "tabliveStock", "/xcj/livestock/changeFlag", "getLivestockList", "liveStock")
      //工具行操作（审核提交/退回修改/批量删除/新增/复制新增）
      handleChart.toolbarPrice($, form, table, 'toolbar(test)', "没有选中指定的信息", 'exampleEdit', urlObj, "/xcj/livestock/findLivestockList", "tabliveStock", ["/xcj/livestock/changeState", "/xcj/livestock/changeFlag", "/xcj/livestock/addLivestock"], "getLivestockList", "liveStock");
      //遍历获取品种及表单默认数据。
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findLivestockList", "/xcj/breed/findCityList"], "demo1", "queryBox", ["选择品种", "行政单位"], queryliveStock, "queryDate")

      //获取后台默认数据，构建新增表单模板
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findLivestockList", "/xcj/breed/findCityList"], "demo4", "std", ["选择品种", "行政单位"], addStock, "")

      //获取后台默认数据，构建新增表单模板
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findLivestockList", "/xcj/breed/findCityList"], "demo5", "etd", ["选择品种", "行政单位"], editStock, "")

      //查询表单(信息录入)
      handleChart.getLivestockList($, table, urlObj, "/xcj/livestock/findLivestockList", "tabliveStock");
      //退出登录
      tool.exitHandle($, "exitBtn", ["id", "bazaarIds", "token", "username"], "login.html");
      //根据表单条件查询数据（信息录入/汇总统计）
      handleChart.submitTip($, table, form, "demo1", "/xcj/livestock/findLivestockList", handleChart['getLivestockList'], "tabliveStock", "liveStock")

      //新增一条数据
      handleChart.submitAddPrice($, table, form, "demo4", handleChart['putAddInfo'], "/xcj/livestock/addLivestock", "tabliveStock", "/xcj/livestock/findLivestockList",  "getLivestockList", "liveStock")

      //编辑一条数据
      handleChart.submitAddPrice($, table, form, "demo5", handleChart['putEditInfo'], "/xcj/livestock/updateLivestockById", "tabliveStock", "/xcj/livestock/findLivestockList",  "getLivestockList", "liveStock")

      //全选或常用
      handleChart.checkSwitch($, form, laydate, "checkbox(c_one)", 'switch(switchTest)', "#c_all", ".checkSelt", 'checkbox', "checked", queryliveStock, "getVariety", "demo1", ["/xcj/breed/findLivestockList", "/xcj/breed/findCityList"], "queryBox", ["选择品种", "行政单位"], "queryDate")

    });


});
