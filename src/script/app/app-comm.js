"use strict";
require([
    './dev/layer',
    // './dev/mockObj',
    './dev/commonHandle',
    './dev/tool',
    'text!tpl/InfoEnter.hbs',
    './dev/commpany'
], function (layFrame, commonHandle, tool, enTpl, compy) {

    layui.use(['layer', 'form', 'element', 'table'],  () => {
        const  $ = layui.jquery,
          layer = layui.layer,
          element = layui.element,
          table = layui.table,
          form = layui.form;

        // mock.setEnter(compy)

      // commonHandle.getEnterprise($, "/data/info", enTpl, "enInfo")

      commonHandle.setUserName($, "#userName");

      commonHandle.getEnterprise($, "/enterprise/selectDeclareInfo", enTpl, "enInfo")

      commonHandle.exitHandle($, "exitBtn", ["id", "userName", "enterpriseId"], "login.html")

    });


});
