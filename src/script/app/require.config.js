"use strict";

const baseUrl = "./js";

const waitSeconds = 0;

const urlArgs = '_=' + new Date().getTime();

const cdnjs =`https://cdnjs.cloudflare.com/ajax/libs/`

const paths = {
  'jquery': [
    `${cdnjs}jquery/1.7/jquery.min`,
    //"https://cdnjs.cloudflare.com/ajax/libs/jquery/1.7/jquery.min",
    // "https://cdn.bootcss.com/jquery/3.1.1/jquery",
    `${cdnjs}jquery/3.1.1/jquery`,
    "./vendor/jquery/dist/jquery"
  ],
  'backbone': [
    `${cdnjs}backbone.js/1.4.0/backbone`
    // "https://cdn.bootcss.com/backbone.js/1.4.0/backbone"
  ],
  'director':[
    `${cdnjs}Director/1.2.8/director`
  ],
  'underscore': [
    `${cdnjs}underscore.js/1.9.1/underscore`
    //"https://cdn.bootcss.com/underscore.js/1.9.1/underscore"
  ],
  'mock':[
    `${cdnjs}Mock.js/1.0.0/mock-min`
  ],
  'bootstrap': [
    "./vendor/bootstrap/dist/js/bootstrap"
  ],
  'html2canvas': [
    // "https://cdn.bootcss.com/html2canvas/0.5.0-beta4/html2canvas"
    `${cdnjs}html2canvas/0.5.0-beta4/html2canvas`
  ],
  'css': [
    "./vendor/require-css/css"
  ],
  'text': [
    "./vendor/text/text"
  ],
  'jquery-ui': [
    //"https://cdn.bootcss.com/jqueryui/1.12.1/jquery-ui.min"
      `${cdnjs}jqueryui/1.12.1/jquery-ui.min`
  ],
  'openlayers': [
    "https://openlayers.org/en/v4.6.4/build/ol"
  ],
  'crypto-js':[
    `${cdnjs}crypto-js/3.1.9-1/crypto-js.min`
  ],
  'handlebars': [
    `${cdnjs}handlebars.js/4.0.11/handlebars.min`
    //"https://cdn.bootcss.com/handlebars.js/4.0.11/handlebars.min"
  ],
  'echarts': [
    `${cdnjs}echarts/4.1.0.rc2/echarts`
    //"https://cdn.bootcss.com/echarts/4.1.0.rc2/echarts"
  ],
  'layui': [
    "./vendor/layui/src/layui"
  ],
  'polyfill': [
    `${cdnjs}babel-polyfill/7.4.3/polyfill.min`
    //"https://cdn.bootcss.com/babel-polyfill/7.2.5/polyfill.min"
  ],
  'pagination': [
    `${cdnjs}paginationjs/2.1.4/pagination.min`
    //"https://cdn.bootcss.com/paginationjs/2.1.4/pagination.min"
  ],
  'swiper3': [
    `${cdnjs}Swiper/3.4.2/js/swiper.min`
    //"https://cdn.bootcss.com/Swiper/3.4.2/js/swiper.min"
  ],
  'swiper4': [
    `${cdnjs}Swiper/4.5.0/js/swiper.min`
    //"https://cdn.bootcss.com/Swiper/4.5.0/js/swiper.min"
  ],
  'modernizr': [
    `${cdnjs}modernizr/2.5.3/modernizr.min`
    //"https://cdn.bootcss.com/modernizr/2.5.3/modernizr.min"
  ],
  'turn': [
    `${cdnjs}turn.js/3/turn.min`
    //"https://cdn.bootcss.com/turn.js/3/turn.min"
  ],
  'zoom': [
    `${cdnjs}zoom.js/0.0.1/zoom.min`
    //"https://cdn.bootcss.com/zoom.js/0.0.1/zoom.min"
  ],
  'BMap': [
    "http://api.map.baidu.com/api?v=2.0&ak=lNnmM8dmovZvKZ2TGKtFw0znVdM469p0"
  ],
  'hash': [
    "./lib/hash"
  ],
  'magazine': [
    "./lib/magazine"
  ],
  'manifest': [
    "./lib/manifest"
  ],
  'star': [
    "./lib/star"
  ],
  'lrtk': [
    "./lib/lrtk"
  ]
};

const shim = {
  'bootstrap': [
    `${cdnjs}jquery/3.1.1/jquery.js`,
    //"https://cdn.bootcss.com/jquery/3.1.1/jquery.js",
    "css!./vendor/bootstrap/dist/css/bootstrap.min.css",
    `css!${cdnjs}font-awesome/4.7.0/css/font-awesome.min.css`,
    //"css!https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css",
    "css!../css/style.css",
    "css!../css/index.css"
  ],
  'backbone':[
    "underscore"
  ],
  'jquery-ui': [
    "jquery",
    `css!${cdnjs}jqueryui/1.12.1/jquery-ui.min.css`
    //"css!https://cdn.bootcss.com/jqueryui/1.12.1/jquery-ui.min.css",
  ],
  'openlayers': [
    "css!https://openlayers.org/en/v4.6.4/css/ol.css",
    "css!../css/index.css"
  ],
  'pagination': [
    'jquery',
    `css!${cdnjs}paginationjs/2.1.4/pagination.css`
    //"css!https://cdn.bootcss.com/paginationjs/2.1.4/pagination.css"
  ],
  'lrtk': [
    "jquery"
  ],
  'magazine': [
    "jquery",
    "css!../css/index.css"
  ],
  'handlebars': [
    `css!${cdnjs}paginationjs/2.1.4/pagination.css`
    //"css!https://cdn.bootcss.com/paginationjs/2.1.4/pagination.css"
  ],
  'swiper3': [
    `css!${cdnjs}Swiper/3.4.2/css/swiper.min.css`
    //"css!https://cdn.bootcss.com/Swiper/3.4.2/css/swiper.min.css"
  ],
  'swiper4': [
    `css!${cdnjs}Swiper/4.5.0/css/swiper.min.css`
    //"css!https://cdn.bootcss.com/Swiper/4.5.0/css/swiper.min.css"
  ],
  'modernizr': {
    exports: 'Modernizr'
  },
  'hash': {
    exports: 'Hash'
  },
  'turn': {
    deps: [
      "jquery"
    ]
  },
  'zoom': {
    deps: [
      "jquery"
    ]
  },
  'BMap': {
    exports: 'BMap'
  },
  'layui': {
    exports: 'layui'
  },
  'jquery1': {
    exports: '$'
  }
};

const config = {
  text: {
    onXhr(xhr, url) {
      xhr.setRequestHeader('X-Requested-with', 'XMLHttpRequest')
    },
    createXhr() {

    },
    onXhrComplete(xhr, url) {

    }
  }
};

const map = {
  // '*': {
  //   'jquery': "https://cdn.bootcss.com/jquery/3.1.1/jquery.js"
  // },
  // 'dev/book': {
  //   'jquery': "https://cdn.bootcss.com/jquery/1.7/jquery.min.js"
  // }
};

require.config({
  baseUrl,
  //urlArgs,
  paths,
  shim,
  map,
  //config
});
