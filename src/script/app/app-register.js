"use strict";
require([
    './dev/layer',
    './conf/baseConfig',
    './dev/register',
    './dev/user',
    'text!tpl/register.hbs',
    './dev/http-lay',
    './dev/commoRegister',
], function (layFrame, baseConfig, registerMod, userMod, register, http, commoRegister) {

    layui.use(['layer', 'form', 'element', 'upload'],  () => {
        const  $ = layui.jquery,
          layer = layui.layer,
          element = layui.element,
          upload = layui.upload,
          form = layui.form;

        let reg,user,iphone;

      commoRegister["uploadfile"]($, upload, '#testList', '/batch/upload', '#testListAction', "#demoList").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
            arrSrc.push(item.src)
        })
        reg.enterpriseScale = arrSrc.join()
        console.log(reg)
        if(arrSrc.length!==0){
          commoRegister.tickShowHid($, "#step0", "#upload0", "#upload1")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commoRegister["uploadfile"]($, upload, '#testList1', '/batch/upload', '#testListAction1', "#demoList1").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        reg.enterpriseBenefit = arrSrc.join()
        console.log(reg)
        if(arrSrc.length!==0){

          commoRegister.tickShowHid($, "#step1", "#upload1", "#upload2")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commoRegister["uploadfile"]($, upload, '#testList2', '/batch/upload', '#testListAction2', "#demoList2").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        reg.enterpriseHonesty = arrSrc.join()
        console.log(reg)
        if(arrSrc.length!==0){
          commoRegister.tickShowHid($, "#step2", "#upload2", "#upload3")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commoRegister["uploadfile"]($, upload, '#testList3', '/batch/upload', '#testListAction3', "#demoList3").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        reg.baseScale= arrSrc.join()
        console.log(reg)
        if(arrSrc.length!==0){
          commoRegister.tickShowHid($, "#step3", "#upload3", "#upload4")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commoRegister["uploadfile"]($, upload, '#testList4', '/batch/upload', '#testListAction4', "#demoList4").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        reg.bandScale= arrSrc.join()
        console.log(reg)
         let sumObj ={
          enterpriseUser:user,
          enterpriseInfo:reg
          }
        commoRegister.submitRegister($, "#subRes", sumObj, "/enterprise/addEnterpriseUser")
      });



      commoRegister.getVariety($, form, "/enterprise/selectInformation", "formInfo", register, "select")

      form.on('submit(submitId)', function (data) {
        if(data){
          if(data.field.password === data.field.aspasswd){
            $("#formInfo").hide()
            $("#upload0").show()
            user = new userMod(data.field.username, data.field.password)
            reg  = new registerMod(data.field.fullname, iphone, data.field.construct, data.field.areas, data.field.enType)
          } else {
            layFrame.showMsg("两次输入的密码不一致", 0, 3000);
          }

        }
        return false
      })

      form.on('submit(verifyTel)', function (data) {
        if(data){
          // console.log(data)
          let obj ={
            tel:data.field.telephone,
            code:data.field.verifyCode
          }
          commoRegister.checkVerifyCode($, "/enterprise/getMsg", obj).then((res)=>{
            if(res){
              // console.log(res.code)
              switch (res.code) {
                case 0:layFrame.showMsg("手机号验证失败，请确认手机号是否正确", 0, 3000);
                  break;
                case 1:layFrame.showMsg("手机号验证成功，请继续填写注册信息", 1, 3000);
                  $(".verifyTel").hide()
                  $("#formInfo").show()
                  iphone = data.field.telephone;
                  break;
                case 9:layFrame.showMsg("网络异常，请稍后重新获取验证码", 0, 3000);
                  break;
              }
            }
          })
        }
        return false
      })

      commoRegister.checkUserName($, "#userName", "/selectUserName")

      //点击发送验证码按钮变化
      commoRegister.clickVerifyCode($, "#getAuthCodeBtn", "/enterprise/setMsg", $("#phoneNum").val())

      commoRegister.checkinputVal(form)

    });


});
