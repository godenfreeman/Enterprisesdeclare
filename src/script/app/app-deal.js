"use strict";
require([
    //'polyfill',
    './dev/layer',
    './dev/http-lay',
    './dev/tool',
    'text!tpl/varieties.hbs',
    './conf/urlObj',
    './dev/handleChart'
], function ( layFrame, http, tool, varieties, urlObj, handleChart) {


    layui.use(['layer','table', 'element', 'form', 'laydate'],  () => {
      const  $ = layui.jquery,
        table = layui.table,
        element = layui.element,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;


      //遍历获取品种及表单默认数据。
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "demo1", "queryBox", ["选择品种", "选择市场"], varieties, "queryDate")
      handleChart.getSumExcList($, table, urlObj, "/xcj/vegetables/findVolumeStatement", "tabVolExc");
      //退出登录
      tool.exitHandle($, "exitBtn", ["id", "bazaarIds", "token", "username"], "login.html")
      //根据表单条件查询数据（信息录入/汇总统计）
      handleChart.submitTip($, table, form, "demo1", "/xcj/vegetables/findVolumeStatement", handleChart['getSumExcList'], "tabVolExc")
      //全选或常用
      handleChart.checkSwitch($, form, laydate, "checkbox(c_one)", 'switch(switchTest)', "#c_all", ".checkSelt", 'checkbox', "checked", varieties, "getVariety", "demo1", ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "queryBox", ["选择品种", "选择市场"], "queryDate")

    });


});
