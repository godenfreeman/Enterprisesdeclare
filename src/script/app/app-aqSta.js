"use strict";
require([
    './dev/tool',
    'text!tpl/queryInfo.hbs',
    'text!tpl/addtip.hbs',
    './conf/urlObj',
    './dev/handleChart',
    'mock'
], function (tool, queryInfo, addtip, urlObj, handleChart, Mock) {

    layui.use(['layer','table', 'element', 'form', 'laydate'],  () => {
      const  $ = layui.jquery,
        table = layui.table,
        element = layui.element,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;


      // 单行操作（编辑/删除）
      //handleChart.handeTool($, form, table,'tool(test)', '真的删除行么', 'exampleEdit', "/xcj/vegetables/listInfo", "tabConf", "/xcj/vegetables/changeFlag")
      //工具行操作（审核提交/退回修改/批量删除/新增/复制新增）
      //handleChart.toolbarPrice($, form, table, 'toolbar(test)', "没有选中指定的信息", 'exampleEdit', urlObj, "/xcj/vegetables/listInfo", "tabConf", ["/xcj/vegetables/changeState", "/xcj/vegetables/changeFlag", "/xcj/vegetables/addVegetable"]);
      //遍历获取品种及表单默认数据。
      //handleChart.getVariety($, form, laydate, ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "demo1", "queryBox", ["选择品种", "选择市场"], queryInfo, "queryDate")
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findAquaticList", "/xcj/breed/findbazaarList"], "demo2", "censusBox", ["选择品种", "选择市场"], queryInfo, "censusDate")
      //获取后台默认数据，构建新增表单模板
      //handleChart.getVariety($, form, laydate, ["/xcj/breed/findAquaticList", "/xcj/breed/findbazaarList"], "demo4", "std", ["选择品种", "选择市场"], addtip, "")
      //查询表单(信息录入)
      //handleChart.getPriceList($, table, urlObj, "/xcj/vegetables/listInfo", "tabConf");
      //查询表单(汇总统计)
      handleChart.getStatistisData($, table, urlObj, "/xcj/aproduct/findByStatistisData", "tabSta");
      //退出登录
      tool.exitHandle($, "exitBtn", ["id", "bazaarIds", "token", "username"], "login.html");
      //根据表单条件查询数据（信息录入/汇总统计）
      handleChart.submitTip($, table, form, "demo1", "/xcj/vegetables/listInfo", handleChart['getPriceList'], "tabConf")
      //handleChart.submitTip($, table, form, "demo2", "/xcj/vegetables/findByStatistisData", handleChart['getStatistisData'], "tabSta")
      //新增一条数据
      handleChart.submitAddPrice($, table, form, "demo4", handleChart['putAddInfo'], "/xcj/vegetables/addVegetable", "tabConf", "/xcj/vegetables/listInfo")
      //全选或常用
      handleChart.checkSwitch($, form, laydate, "checkbox(c_one)", 'switch(switchTest)', "#c_all", ".checkSelt", 'checkbox', "checked", queryInfo, "getVariety", "demo2", ["/xcj/breed/findAquaticList", "/xcj/breed/findbazaarList"], "censusBox", ["选择品种", "选择市场"], "censusDate")
    });


});
