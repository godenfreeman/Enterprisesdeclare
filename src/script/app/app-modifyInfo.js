"use strict";
require([
  './dev/layer',
  './conf/baseConfig',
  './dev/register',
  './dev/user',
  'text!tpl/modify.hbs',
  './dev/http-lay',
  './dev/commonHandle',
  "./conf/urlObj"
], function (layFrame, baseConfig, registerMod, userMod, modify, http, commonHandle, urlObj) {

  layui.use(['layer', 'form', 'element', 'upload'],  () => {
    const  $ = layui.jquery,
      layer = layui.layer,
      element = layui.element,
      upload = layui.upload,
      form = layui.form;

    // commonHandle["uploadfile"]($, upload, '#testList', '/batch/upload', '#testListAction', "#demoList").then((data)=>{
    //   let arrSrc =[];
    //   data.map((item, index)=>{
    //     arrSrc.push(item.src)
    //   })
    //   reg.enterpriseScale = arrSrc.join()
    //   console.log(reg)
    //   if(arrSrc.length!==0){
    //     commonHandle.tickShowHid($, "#step0", "#upload0", "#upload1")
    //   } else {
    //     layFrame.showMsg("上传文件不能为空", 0, 3000)
    //   }
    // })
    //
    // commonHandle["uploadfile"]($, upload, '#testList1', '/batch/upload', '#testListAction1', "#demoList1").then((data)=>{
    //   let arrSrc =[];
    //   data.map((item, index)=>{
    //     arrSrc.push(item.src)
    //   })
    //   reg.enterpriseBenefit = arrSrc.join()
    //   console.log(reg)
    //   if(arrSrc.length!==0){
    //
    //     commonHandle.tickShowHid($, "#step1", "#upload1", "#upload2")
    //   } else {
    //     layFrame.showMsg("上传文件不能为空", 0, 3000)
    //   }
    // })
    //
    // commonHandle["uploadfile"]($, upload, '#testList2', '/batch/upload', '#testListAction2', "#demoList2").then((data)=>{
    //   let arrSrc =[];
    //   data.map((item, index)=>{
    //     arrSrc.push(item.src)
    //   })
    //   reg.enterpriseHonesty = arrSrc.join()
    //   console.log(reg)
    //   if(arrSrc.length!==0){
    //     commonHandle.tickShowHid($, "#step2", "#upload2", "#upload3")
    //   } else {
    //     layFrame.showMsg("上传文件不能为空", 0, 3000)
    //   }
    // })
    //
    // commonHandle["uploadfile"]($, upload, '#testList3', '/batch/upload', '#testListAction3', "#demoList3").then((data)=>{
    //   let arrSrc =[];
    //   data.map((item, index)=>{
    //     arrSrc.push(item.src)
    //   })
    //   reg.baseScale= arrSrc.join()
    //   console.log(reg)
    //   if(arrSrc.length!==0){
    //     commonHandle.tickShowHid($, "#step3", "#upload3", "#upload4")
    //   } else {
    //     layFrame.showMsg("上传文件不能为空", 0, 3000)
    //   }
    // })
    //
    // commonHandle["uploadfile"]($, upload, '#testList4', '/batch/upload', '#testListAction4', "#demoList4").then((data)=>{
    //   let arrSrc =[];
    //   data.map((item, index)=>{
    //     arrSrc.push(item.src)
    //   })
    //   reg.bandScale= arrSrc.join()
    //   console.log(reg)
    //   let sumObj ={
    //     enterpriseUser:user,
    //     enterpriseInfo:reg
    //   }
    //   commonHandle.submitRegister($, "#subRes", sumObj, "/enterprise/addEnterpriseUser")
    // });


    commonHandle.setUserName($, "#userName");

    commonHandle.exitHandle($, "exitBtn", ["id", "userName", "enterpriseId"], "login.html")

    commonHandle.getVariety($, form, "/enterprise/selectInformation", "enInfo", modify, "select")

    form.on('submit(submitId)', function (data) {
      if(data){
        let obj  = {
          id:urlObj.enterpriseId,
          linkman:data.field.construct,
          phone: data.field.phone,
          enterpriseArea:data.field.areas,
          enterpriseType:data.field.enType
        }
        let sumObj ={
          enterpriseInfo:obj
        }
        commonHandle.submitRegister($, "", sumObj, "/enterprise/addEnterpriseUser")
      }
      return false
    })

    commonHandle.checkinputVal(form)

  });


});
