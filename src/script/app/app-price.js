"use strict";
require([
    './dev/tool',
    './dev/mockSet',
    'text!tpl/queryInfo.hbs',
    'text!tpl/addtip.hbs',
    'text!tpl/editTip.hbs',
    './conf/urlObj',
    './conf/baseConfig',
    './dev/handleChart',
    'mock'
], function (tool, mockSet, queryInfo, addtip, editTip, urlObj, baseConfig, handleChart, Mock) {

    layui.use(['layer','table', 'element', 'form', 'laydate', 'upload'],  () => {
      const  $ = layui.jquery,
        table = layui.table,
        element = layui.element,
        layer = layui.layer,
        laydate = layui.laydate,
        upload = layui.upload,
        form = layui.form;


      //mockSet.setMock()
      // handleChart.downloadExcel($, "#downExc", "/data/index")


      //选完文件后不自动上传
      // upload.render({
      //   elem: '#test8'
      //   ,url: baseConfig.serUrl+'/xcj/excel/importVegetablesTemplate'
      //   ,data:{bazaarId:17,userName:"manager"}
      //   ,method: 'put'
      //   ,headers: {token: urlObj.token}
      //   ,auto: false
      //   //,multiple: true
      //   ,bindAction: '#test9'
      //   ,done: function(res){
      //     console.log(res)
      //   }
      // });

      // const getVarieties = (path, filterBtn, container, titles, date, tpl) =>{
      //   $.get(path, (res)=>{
      //     let obj = {
      //       filter:filterBtn,
      //       date:date,
      //       varieties:{
      //         title:titles[0],
      //         tip:res.varieties
      //       },
      //       market:{
      //         title:titles[1],
      //         tip:res.bazaar
      //       }
      //     };
      //     layFrame.setlayTpl("laytpl", container, tpl, obj)
      //   })

      //   //日期
      //   laydate.render({
      //     elem: `#${date}`
      //     ,range: true
      //   });
      // }

      // const Random = Mock.Random
      //
      // console.log(Random.range(10))
      //
      // const data = Mock.mock({
      //   "data|2-10": {
      //     "code": "String:7",
      //     "percent": "String:7",
      //     "tels|2-5": "Number:13|x.xxxxxxxxxxxxxxx",
      //     "array|5-10": "String:7-12",
      //     "time": "Date|YYYY-MM-DD",
      //     "String:4|2-5": {
      //       "name": "String:7|xxx.xxxx",
      //       "time": "String:7",
      //       "String:2-5": "Number:10"
      //     },
      //     "avatar": "Image|200x200",
      //     "thumbnails|2-5": "Image|200x300-400x500"
      //   }
      // })
      //






      // 单行操作（编辑/删除）
      handleChart.handeTool($, form, table,'tool(test)', '真的删除行么', 'exampleEdit', "/xcj/vegetables/listInfo", "tabConf", "/xcj/vegetables/changeFlag", "getPriceList", "vegetable")
      //工具行操作（审核提交/退回修改/批量删除/新增/复制新增）
      handleChart.toolbarPrice($, form, table, 'toolbar(test)', "没有选中指定的信息", 'exampleEdit', urlObj, "/xcj/vegetables/listInfo", "tabConf", ["/xcj/vegetables/changeState", "/xcj/vegetables/changeFlag", "/xcj/vegetables/addVegetable"], "getPriceList", "vegetable");
      //遍历获取品种及表单默认数据。
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "demo1", "queryBox", ["选择品种", "选择市场"], queryInfo, "queryDate")
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "demo2", "censusBox", ["选择品种", "选择市场"], queryInfo, "censusDate")
      //获取后台默认数据，构建新增表单模板
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "demo4", "std", ["选择品种", "选择市场"], addtip, "")
      //获取后台默认数据，构建编辑表单模板
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "demo5", "etd", ["选择品种", "选择市场"], editTip, "")
      //查询表单(信息录入)
      handleChart.getPriceList($, table, urlObj, "/xcj/vegetables/listInfo", "tabConf");
      //查询表单(汇总统计)
      handleChart.getStatistisData($, table, urlObj, "/xcj/vegetables/findByStatistisData", "tabSta");
      //退出登录
      tool.exitHandle($, "exitBtn", ["id", "bazaarIds", "token", "username"], "login.html");
      //根据表单条件查询数据（信息录入/汇总统计）
      handleChart.submitTip($, table, form, "demo1", "/xcj/vegetables/listInfo", handleChart['getPriceList'], "tabConf", "vegetable")
      handleChart.submitTip($, table, form, "demo2", "/xcj/vegetables/findByStatistisData", handleChart['getStatistisData'], "tabSta", "vegetable")
      //新增一条数据
      handleChart.submitAddPrice($, table, form, "demo4", handleChart['putAddInfo'], "/xcj/vegetables/addVegetable", "tabConf", "/xcj/vegetables/listInfo", "getPriceList", "vegetable")

      //编辑一条数据
      handleChart.submitAddPrice($, table, form, "demo5", handleChart['putEditInfo'], "/xcj/vegetables/updateVegetable", "tabConf", "/xcj/vegetables/listInfo",  "getPriceList", "vegetable")
      //全选或常用
      handleChart.checkSwitch($, form, laydate, "checkbox(c_one)", 'switch(switchTest)', "#c_all", ".checkSelt", 'checkbox', "checked", queryInfo, "getVariety", "demo1", ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "queryBox", ["选择品种", "选择市场"], "queryDate")
      //handleChart.checkSwitch($, form, laydate, "checkbox(c_one)", 'switch(switchTest)', "#c_all", ".checkSelt", 'checkbox', "checked", queryInfo, "getVariety", "demo2", ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "censusBox", ["选择品种", "选择市场"], "censusDate")


    });


});
