"use strict";
require([
    './dev/layer',
    './conf/baseConfig',
    './dev/register',
    './dev/user',
    './dev/tool',
    'text!tpl/register.hbs',
    './dev/http-lay',
    './dev/commonHandle',
    './conf/urlObj'
], function (layFrame, baseConfig, registerMod, userMod, tool, register, http, commonHandle, urlObj) {

    layui.use(['layer', 'form', 'element', 'upload'],  () => {
        const  $ = layui.jquery,
          layer = layui.layer,
          element = layui.element,
          upload = layui.upload,
          form = layui.form;

        let upObj={};

      commonHandle["uploadfile"]($, upload, '#testList0', '/batch/upload', '#testListAction0', "#demoList0").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.report = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step0", "#upload0", "#upload1")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList1', '/batch/upload', '#testListAction1', "#demoList1").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.introduce = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step1", "#upload1", "#upload2")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList2', '/batch/upload', '#testListAction2', "#demoList2").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.license = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step2", "#upload2", "#upload3")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList3', '/batch/upload', '#testListAction3', "#demoList3").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.finance = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step3", "#upload3", "#upload4")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList4', '/batch/upload', '#testListAction4', "#demoList4").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.tax = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step4", "#upload4", "#upload5")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList5', '/batch/upload', '#testListAction5', "#demoList5").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.environment = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step5", "#upload5", "#upload6")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList6', '/batch/upload', '#testListAction6', "#demoList6").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.base = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step6", "#upload6", "#upload7")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList7', '/batch/upload', '#testListAction7', "#demoList7").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.attestation = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step7", "#upload7", "#upload8")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList8', '/batch/upload', '#testListAction8', "#demoList8").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.trademark = arrSrc.join()
        console.log(upObj)
        if(arrSrc.length!==0){
          commonHandle.tickShowHid($, "#step8", "#upload8", "#upload9")
        } else {
          layFrame.showMsg("上传文件不能为空", 0, 3000)
        }
      })

      commonHandle["uploadfile"]($, upload, '#testList9', '/batch/upload', '#testListAction9', "#demoList9").then((data)=>{
        let arrSrc =[];
        data.map((item, index)=>{
          arrSrc.push(item.src)
        })
        upObj.bandScale= arrSrc.join()
        upObj.enterpriseId = urlObj.enterpriseId
        console.log(upObj)
        let entDec = {enterpriseDeclare:upObj}
        commonHandle.submitUpFile($, "#subUpRes", entDec, "/enterprise/declareEnterpriseCock")
      });

      commonHandle.setUserName($, "#userName");


    });



});
