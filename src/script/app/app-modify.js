"use strict";
require([
    './dev/layer',
    './conf/baseConfig',
    './dev/register',
    './dev/user',
    './dev/tool',
    'text!tpl/setPasswd.hbs',
    './dev/http-lay',
    './dev/commoRegister',
], function (layFrame, baseConfig, registerMod, userMod, tool, setPasswd, http, commoRegister) {

    layui.use(['layer', 'form', 'element', 'upload'],  () => {
        const  $ = layui.jquery,
          layer = layui.layer,
          element = layui.element,
          upload = layui.upload,
          form = layui.form;

        let iphone;

      commoRegister.getVariety($, form, "/enterprise/selectInformation", "passwdInfo", setPasswd, "select")

      form.on('submit(submitId)', function (data) {
        if(data){
          if(data.field.newpassword === data.field.agpassword){
            let obj ={
              enterpriseUser:{
                phone:iphone,
                password:data.field.newpassword
              }
            };
            commoRegister.submitRegister($, "", obj, "/enterprise/addEnterpriseUser")
          } else {
            layFrame.showMsg("两次输入的密码不一致", 0, 3000);
          }

        }
        return false
      })

      form.on('submit(verifyTel)', function (data) {
        if(data){
          // console.log(data)
          let obj ={
            tel:data.field.telephone,
            code:data.field.verifyCode
          }
          commoRegister.checkVerifyCode($, "/enterprise/getMsg", obj).then((res)=>{
            if(res){
              console.log(res.code)
              switch (1) {
                case 0:layFrame.showMsg("手机号验证失败，请确认手机号是否正确", 0, 3000);
                  break;
                case 1:layFrame.showMsg("手机号验证成功，请继续填写注册信息", 1, 3000);
                  $(".verifyTel").hide()
                  $("#passwdInfo").show()
                  iphone = data.field.telephone;
                  break;
                case 9:layFrame.showMsg("网络异常，请稍后重新获取验证码", 0, 3000);
                  break;
              }
            }
          })
        }
        return false
      })

      //点击发送验证码按钮变化
      commoRegister.clickVerifyCode($, "#getAuthCodeBtn", "/enterprise/setMsg", $("#phoneNum").val())

      commoRegister.checkinputVal(form)

    });


});
