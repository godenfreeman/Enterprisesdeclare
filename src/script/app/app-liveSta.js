"use strict";
require([
    './dev/tool',
    'text!tpl/varieties.hbs',
    'text!tpl/addtip.hbs',
    './conf/urlObj',
    './dev/handleChart',
    'mock'
], function (tool, varieties, addtip, urlObj, handleChart, Mock) {

    layui.use(['layer','table', 'element', 'form', 'laydate'],  () => {
      const  $ = layui.jquery,
        table = layui.table,
        element = layui.element,
        layer = layui.layer,
        laydate = layui.laydate,
        form = layui.form;


      // 单行操作（编辑/删除）
      //handleChart.handeTool($, form, table,'tool(test)', '真的删除行么', 'exampleEdit', "/xcj/vegetables/listInfo", "tabConf", "/xcj/vegetables/changeFlag")
      //工具行操作（审核提交/退回修改/批量删除/新增/复制新增）
      //handleChart.toolbarPrice($, form, table, 'toolbar(test)', "没有选中指定的信息", 'exampleEdit', urlObj, "/xcj/livestock/findLivestockList", "tabliveStock", ["/xcj/vegetables/changeState", "/xcj/vegetables/changeFlag", "/xcj/vegetables/addVegetable"], "getLivestockList", "liveStock");
      //遍历获取品种及表单默认数据。
      //handleChart.getVariety($, form, laydate, ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "demo1", "queryBox", ["选择品种", "选择市场"], queryInfo, "queryDate")
      handleChart.getVariety($, form, laydate, ["/xcj/breed/findLivestockList", "/xcj/breed/findCityList"], "demo2", "censusBox", ["选择品种", "选择市场"], varieties, "censusDate")
      //获取后台默认数据，构建新增表单模板
      //handleChart.getVariety($, form, laydate, ["/xcj/breed/findVegtableList", "/xcj/breed/findbazaarList"], "demo4", "std", ["选择品种", "选择市场"], addtip, "")
      //查询表单(信息录入)
      //handleChart.getPriceList($, table, urlObj, "/xcj/vegetables/listInfo", "tabConf");
      //查询表单(汇总统计)
      handleChart.getStatistisData($, table, urlObj, "/xcj/livestock/findByStatistisData", "tabSta");
      //退出登录
      tool.exitHandle($, "exitBtn", ["id", "bazaarIds", "token", "username"], "login.html");
      //根据表单条件查询数据（信息录入/汇总统计）
      //handleChart.submitTip($, table, form, "demo1", "/xcj/vegetables/listInfo", handleChart['getPriceList'], "tabConf")
      handleChart.submitTip($, table, form, "demo2", "/xcj/livestock/findByStatistisData", handleChart['getStatistisData'], "tabSta", "liveStock")
      //新增一条数据
      handleChart.submitAddPrice($, table, form, "demo4", handleChart['putAddInfo'], "/xcj/vegetables/addVegetable", "tabConf", "/xcj/vegetables/listInfo")
      //全选或常用
      handleChart.checkSwitch($, form, laydate, "checkbox(c_one)", 'switch(switchTest)', "#c_all", ".checkSelt", 'checkbox', "checked", varieties, "getVariety", "demo2", ["/xcj/breed/findLivestockList", "/xcj/breed/findCityList"], "censusBox", ["选择品种", "选择市场"], "censusDate")
    });


});
