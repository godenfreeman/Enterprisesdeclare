"use strict";
require([
    './dev/layer',
    './conf/baseConfig',
    // './dev/tool',
    'text!tpl/banner.hbs',
    './dev/http-lay'
], function (layFrame, baseConfig, banner, http) {
    //layer.loadLayer();
    layui.use(['layer','laypage', 'form', 'element'],  () => {
        const  $ = layui.jquery,
          layer = layui.layer,
          form = layui.form;
        let show_num = [];

      const data = {
          imgSrc:['./images/07.png', './images/08.png', './images/09.jpg']
      };

      layFrame.setlayTpl('laytpl', 'bannerBox', banner, data);
      layFrame.setBanner('carousel', '#bannerCar', '100%', '100%', 'auto');

      //自定义验证规则
      form.verify({
        username: function(value){
          if(value.length < 5){
            return '用户名至少得5个字符啊';
          }
        }
        ,pass: [
          /^[\S]{6,12}$/
          ,'密码必须6到12位，且不能出现空格'
        ]
        ,verifiyCode:function(value){
          if(!value){
            return '请输入验证码';
          } else if (value.toLowerCase() !== show_num.join("")){
            draw(show_num);
            return '您输入的验证码有误！';
          }
        }
      });



      function draw(show_num) {
        const canvas_width=$('#canvas').width();
        const canvas_height=$('#canvas').height();
        const canvas = document.getElementById("canvas");//获取到canvas的对象，演员
        const context = canvas.getContext("2d");//获取到canvas画图的环境，演员表演的舞台
        canvas.width = canvas_width;
        canvas.height = canvas_height;
        const sCode = "A,B,C,E,F,G,H,J,K,L,M,N,P,Q,R,S,T,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
        let aCode = sCode.split(",");
        let aLength = aCode.length;//获取到数组的长度

        for (let i = 0; i <= 3; i++) {
          let j = Math.floor(Math.random() * aLength);//获取到随机的索引值
          let deg = Math.random() * 30 * Math.PI / 180;//产生0~30之间的随机弧度
          let txt = aCode[j];//得到随机的一个内容
          show_num[i] = txt.toLowerCase();
          let x = 10 + i * 20;//文字在canvas上的x坐标
          let y = 20 + Math.random() * 8;//文字在canvas上的y坐标
          context.font = "bold 23px 微软雅黑";

          context.translate(x, y);
          context.rotate(deg);

          context.fillStyle = randomColor();
          context.fillText(txt, 0, 0);

          context.rotate(-deg);
          context.translate(-x, -y);
        }
        for (let i = 0; i <= 5; i++) { //验证码上显示线条
          context.strokeStyle = randomColor();
          context.beginPath();
          context.moveTo(Math.random() * canvas_width, Math.random() * canvas_height);
          context.lineTo(Math.random() * canvas_width, Math.random() * canvas_height);
          context.stroke();
        }
        for (let i = 0; i <= 30; i++) { //验证码上显示小点
          context.strokeStyle = randomColor();
          context.beginPath();
          let x = Math.random() * canvas_width;
          let y = Math.random() * canvas_height;
          context.moveTo(x, y);
          context.lineTo(x + 1, y + 1);
          context.stroke();
        }
      }

      function randomColor() {//得到随机的颜色值
        let r = Math.floor(Math.random() * 256);
        let g = Math.floor(Math.random() * 256);
        let b = Math.floor(Math.random() * 256);
        return "rgb(" + r + "," + g + "," + b + ")";
      }

      draw(show_num);

      $("#canvas").on('click',function(){
        draw(show_num);
      })

      //监听提交
      form.on('submit(demo1)', function(data){
        const obj = {
          userName:data.field.username,
          password:data.field.password
        };
        loginIndex(obj)
      });

      const loginIndex = (obj) =>{
        const params ={
          enterpriseUser:obj
        };
        http.interactive($).commPost(baseConfig.serUrl+"/enterprise/enterpriseUserLogin", params, (res)=>{
          console.log(res)
          if(res.code===1){
            layer.msg(`登录${res.msg}`,{icon: 6})
            let enter = res.data.enterpriseUser
            window.open(`enterList.html?id=${enter.id}&userName=${enter.userName}&enterpriseId=${enter.enterpriseId}`, "_self")
          } else {
            layer.msg(res.msg, {icon: 0,time: 3000})
          }
        }, (err)=>{
          console.log(err)
        }, "")
      }






    });


});
