define(["jquery"],function ($) {
  return {
    loadLink:function(target,showSrc){
      $.ajaxPrefilter( function (options) {
        if (options.crossDomain && jQuery.support.cors) {
          const http = (window.location.protocol === 'http:' ? 'http:' : 'https:');
          options.url = http + '//cors-anywhere.herokuapp.com/' + options.url;
        };
      });

      $.get( showSrc, function (response){
        const html = response;
        const htmlUr = html.replace(/data-src/g, "src").replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/g, '').replace(/https/g,'http');

        const html_src = 'data:text/html;charset=utf-8,' + htmlUr;

        $(target).attr("src" , html_src);

      });
    }
  }
})
