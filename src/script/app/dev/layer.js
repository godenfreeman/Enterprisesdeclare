define(["layui", "conf/layConfig", "conf/cssConf"], function (layui, lays, arrCss) {
  return {
    loadLayer() {
      layui.config(lays)
        .link(arrCss.csslist[0])
        .link(arrCss.csslist[1])
        .link(arrCss.csslist[2])
        .link(arrCss.csslist[3])
        .link(arrCss.csslist[4])
    },
    setlayTpl(mod, id, tpl, data) {
      layui.use(mod, function () {
        const laytpl = layui.laytpl;
        const view = document.getElementById(id);
        laytpl(tpl).render(data, function (html) {
          view.innerHTML = html;
        });
      });

    },
    setBanner(mod, id, width, height, arrow) {
      layui.use(mod, function () {
        var carousel = layui.carousel;
        //建造实例
        carousel.render({
          elem: id,
          width: width, //设置容器宽度
          height: height,
          arrow: arrow //始终显示箭头
          //,anim: 'updown' //切换动画方式
        });
      });
    },
    showBox() {
      layui.use('layer', function () {

        const $ = layui.jquery,
           layer = layui.layer;

        //触发事件
        var active = {
          notice: function (othis) {
           const imgsrc = $(othis.find("img")).attr("src");
            const html = `<div class="elastic"><img src=${imgsrc} width="100%" /></div>`;
            //示范一个公告层
            layer.open({
              type: 1
              , title: false //不显示标题栏
              , closeBtn: false
              , area: '1000px;'
              , shade: 0.8
              , id: 'LAY_layuipro' //设定一个id，防止重复弹出
              , btn: ['关闭']
              , btnAlign: 'c'
              , moveType: 1 //拖拽模式，0或者1
              , content: html
            });
          }
        };

        $(document).on('click', '.showbox', function () {
          var othis = $(this), method = othis.data('method');
          active[method] ? active[method].call(this, othis) : '';
        });


      });
    },
    showStaff(popup, tabObj, list, fun) {

      layui.use(['layer','table'], function () {
        const $ = layui.jquery,
          layer = layui.layer,
          table = layui.table;
        //触发事件
        var active = {
          notice: function (othis) {
            //示范一个公告层
            layer.open(popup);
          }
        };
        const randerTab = function () {
          table.render(tabObj);

          //监听行单击事件（单击事件为：rowDouble）
          table.on('row(test)', function(obj) {
            console.log(list)
            let index = fun(obj.data.key, list, "key")
            sessionStorage.setItem("books", list[index].book)
            window.open("preview.html", "_self")
          });

        };



        $(document).on('click', '.showbox', function () {
          const othis = $(this), method = othis.data('method');
          active[method] ? active[method].call(this, othis) : '';
          randerTab();
        });
      });

    },
    previewTxt(tpl, id, data) {

      layui.use('layer', function () {
        const $ = layui.jquery,
          layer = layui.layer;

        var active = {
          notice: function (othis) {
            layer.open({
              type: 1
              , title: false //不显示标题栏
              , closeBtn: false
              , area: '1000px;'
              , shade: 0.8
              , id: 'LAY_layuipro' //设定一个id，防止重复弹出
              , btn: ['关闭']
              , btnAlign: 'c'
              , moveType: 1 //拖拽模式，0或者1
              , content: `<div id="sumitbox"></div>`
              ,success: function(layero){
                console.log(layero == this)
              }
            });
          }
        };

        var randerTpl = function (tpl, id, data) {
          const laytple = layui.laytpl
          const view = document.getElementById(id);
          laytple(tpl).render(data, function (html) {
            view.innerHTML = html;
          });
        };

        var randerForm = function () {
          const laydate = layui.laydate,
            form = layui.form;
          //日期
          laydate.render({
            elem: '#date'
          });
          //自定义验证规则
          form.verify({
            title: function (value) {
              if (value.length < 5) {
                return '标题至少得5个字符啊';
              }
            }
            , pass: [
              /^[\S]{6,12}$/
              , '密码必须6到12位，且不能出现空格'
            ]
            , content: function (value) {
              layedit.sync(editIndex);
            }
          });

          //监听提交
          form.on('submit(demo1)', function (data) {
            console.log(data)
            layer.alert(JSON.stringify(data.field), {
              title: '最终的提交信息'
            })
            return false;
          });
        }

        $(document).on('click', '.showbox', function () {
          var othis = $(this), method = othis.data('method');
          active[method] ? active[method].call(this, othis) : '';
          randerTpl(tpl, id , data)
          randerForm()
        });



      });



    },
    setLayer() {
      return layui
    },
    showMsg(msg, ico, tim){
      layui.use('layer', function () {
        const layer = layui.layer
        layer.msg(msg,{icon: ico,time: tim})
      });
    }
  }
});
