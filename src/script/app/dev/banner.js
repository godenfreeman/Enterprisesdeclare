define([
  "jquery",
  "swiper3",
  "text!tpl/news.hbs",
  "../dev/tpl"
],function ($, swiper, news, tpl) {
  return {
    loadSwipe(){
      const timelineSwiper = new Swiper('.timeline .swiper-container', {
        direction: 'vertical',
        loop: false,
        speed: 1600,
        mousewheelControl: true,
        pagination: '.swiper-pagination',
        paginationBulletRender: function(swiper, index, className) {
          const year = document.querySelectorAll('.swiper-slide')[index].getAttribute('data-year');
          return '<span class="' + className + '">' + year + '</span>';
        },
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        breakpoints: {
          768: {
            direction: 'horizontal',
          }
        }
      });
    },
    loadTpl(url, target){
      $.getJSON(url, function (data) {
        tpl.CreatTpl(target, news, data);
      })
    }

  }


});
