define([
  "./layer",
  "./http-lay",
  "conf/baseConfig",
  './tool',
  './vegetable',
  './grainOil',
  './aquatic',
  './liveStock',
  './queryObj',
  "conf/urlObj"
],function (layFrame, http, baseConfig, tool, vegetable, grainOil, aquatic, liveStock, queryObj, urlObj) {
    return {
        getVariety ($, eltForm, eltDate, pathArr, filterBtn, container, titles, tpl, date) {   //从后端获取查询条件
            const getVegtable = ()=>{
              const dtd = $.Deferred();
              http.interactive($).commGet(baseConfig.serUrl+pathArr[0], "",(res) => {
                dtd.resolve(res);
              }, (err) => {
                dtd.reject(err)
              }, urlObj.token);
              return dtd.promise();
            };

            getVegtable().then((data)=>{
              http.interactive($).commGet(baseConfig.serUrl+pathArr[1],"", (res) => {
                let obj = {
                  filter:filterBtn,
                  date:date,
                  varieties:{
                    title:titles[0],
                    tip:data
                  },
                  market:{
                    title:titles[1],
                    tip:res
                  }
                };
                //console.log(obj)
                layFrame.setlayTpl("laytpl", container, tpl, obj)
                if(date){
                  eltForm.render()
                  eltDate.render({  //日期
                    elem: `#${date}1`
                    ,range: false
                  });

                  eltDate.render({  //日期
                    elem: `#${date}2`
                    ,range: false
                  });

                }
                }, (err)=>{
                    console.log(err)
                }, urlObj.token)
            });

        },
        getGrainOilList ($, table, obj , url, tabConf, that) {
            const self = this||that;
            const params = {
              bazaarId: obj.bazaarId?obj.bazaarId:null,
              endTime: obj.endTime?obj.endTime:null,
              ids: obj.ids?obj.ids:null,
              pageCurrent: obj.pageCurrent?obj.pageCurrent:null,
              pageSize: obj.pageSize?obj.pageSize:null,
              startTime: obj.startTime?obj.startTime:null
            };
            http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
              let data =[];
              $.each(res.breedSelDtos, (index, item ) => {
                const obj = {
                  pkId: item.pkId,
                  shiChangId: item.shiChangId,
                  bazaarName: item.bazaarName,
                  pinZhongId: item.pinZhongId,
                  typeName: item.typeName,
                  guiGe: item.guiGe,
                  danWei: item.danWei,
                  price: item.price,
                  adduser: item.adduser,
                  addtime: item.addtime,
                  state: tool.handleStatus(item.state)
                };
                data.push(obj)
              });
              self.renderTabeCon(table, data, tool[tabConf])
            }, (err) => {
              console.log(err)
            }, urlObj.token)
        },
        getPriceList ($, table, obj , url, tabConf, that) {
            const self = this||that;
            const params = {
              bazaarId: obj.bazaarId?obj.bazaarId:null,
              endTime: obj.endTime?obj.endTime:null,
              ids: obj.ids?obj.ids:null,
              pageCurrent: obj.pageCurrent?obj.pageCurrent:null,
              pageSize: obj.pageSize?obj.pageSize:null,
              startTime: obj.startTime?obj.startTime:null
            };
            http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
              let data =[];
              $.each(res.breedSelDtos, (index, item ) => {
                const obj = {
                  pkId: item.pkId,
                  shiChangId: item.shiChangId,
                  bazaarName: item.bazaarName,
                  pinZhongId: item.pinZhongId,
                  typeName: item.typeName,
                  jiaoCai: item.jiaoCai,
                  waiCai: item.waiCai,
                  priceMax: item.priceMax,
                  priceLow: item.priceLow,
                  adduser: item.adduser,
                  addtime: item.addtime,
                  state: tool.handleStatus(item.state),
                  pavg: item.pavg
                };
                data.push(obj)
              });
              self.renderTabeCon(table, data, tool[tabConf])
            }, (err) => {
              console.log(err)
            }, urlObj.token)
        },
        getLivestockList ($, table, obj , url, tabConf, that) {
          const self = this||that;
          const params = {
            areaId: obj.areaId?obj.areaId:null,
            endTime: obj.endTime?obj.endTime:null,
            ids: obj.ids?obj.ids:null,
            pageCurrent: obj.pageCurrent?obj.pageCurrent:null,
            pageSize: obj.pageSize?obj.pageSize:null,
            startTime: obj.startTime?obj.startTime:null
          };
          http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
            let data =[];
            $.each(res.breedSelDtos, (index, item ) => {
              const obj = {
                pkId: item.pkId,
                areaId: item.areaId,
                areaName: item.areaName,
                pinZhongId: item.pinZhongId,
                typeName: item.typeName,
                price: item.price,
                adduser: item.adduser,
                addtime: item.addtime,
                state: tool.handleStatus(item.state)
              };
              data.push(obj)
            });
            self.renderTabeCon(table, data, tool[tabConf])
          }, (err) => {
            console.log(err)
          }, urlObj.token)
        },
        getAproductList ($, table, obj , url, tabConf, that) {
          const self = this||that;
          const params = {
            bazaarId: obj.bazaarId?obj.bazaarId:null,
            endTime: obj.endTime?obj.endTime:null,
            ids: obj.ids?obj.ids:null,
            pageCurrent: obj.pageCurrent?obj.pageCurrent:null,
            pageSize: obj.pageSize?obj.pageSize:null,
            startTime: obj.startTime?obj.startTime:null
          };
          http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
            let data =[];
            $.each(res.breedSelDtos, (index, item ) => {
              const obj = {
                pkId: item.pkId,
                shiChangId: item.shiChangId,
                bazaarName: item.bazaarName,
                pinZhongId: item.pinZhongId,
                typeName: item.typeName,
                guiGe: item.guiGe,
                priceMax: item.pmax,
                priceLow: item.plow,
                adduser: item.adduser,
                addtime: item.addtime,
                state: tool.handleStatus(item.state),
                pavg: item.pavg
              };
              data.push(obj)
            });
            self.renderTabeCon(table, data, tool[tabConf])
          }, (err) => {
            console.log(err)
          }, urlObj.token)
        },
        getSumExcList ($, table, obj , url, tabConf, that) {
          const self = this||that;
          const params = {
            endTime: obj.endTime?obj.endTime:null,
            ids: obj.ids?obj.ids:null,
            startTime: obj.startTime?obj.startTime:null
          };
          http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
            let data =[];

            const checkVoluData=(data, target, sign)=>{
              let volu,
                  index = tool.serachIndexData(target, data, "object");
              if(target==="同比"){
                volu = data[index][sign]+"%"
              } else {
                 volu = data[index][sign]
              }

              if(volu===0){
                volu =`&nbsp;`
              }
              return volu
            }

            res.map(item=>{
              const obj = {
                bazaarName: item.bazaarName,
                ciSums: checkVoluData(item.turnoverDtos, "本期", "sum"),
                ciSuburb: checkVoluData(item.turnoverDtos, "本期", "jiaoCai"),
                ciExternal: checkVoluData(item.turnoverDtos, "本期", "waiCai"),
                siSums: checkVoluData(item.turnoverDtos, "同期", "sum"),
                siSuburb: checkVoluData(item.turnoverDtos, "同期", "jiaoCai"),
                siExternal: checkVoluData(item.turnoverDtos, "同期", "waiCai"),
                yoySums: checkVoluData(item.turnoverDtos, "同比", "sum"),
                yoySuburb: checkVoluData(item.turnoverDtos, "同比", "jiaoCai"),
                yoyExternal: checkVoluData(item.turnoverDtos, "同比", "waiCai")
                // qsSuburb: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // qsExternal: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // qsmSums: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // qsmSuburb: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // qsmExternal: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // hpSums: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // hpSuburb: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // hpExternal: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // hjxSums: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // hjxSuburb: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                // hjxExternal: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId)
              }
              data.push(obj)
            })

            self.renderTabeCon(table, data, tool[tabConf])
          }, (err) => {
            console.log(err)
          }, urlObj.token)
        },
        getSummaryList ($, table, obj , url, tabConf, that) {
          const self = this||that;
          const params = {
            endTime: obj.endTime?obj.endTime:null,
            ids: obj.ids?obj.ids:null,
            startTime: obj.startTime?obj.startTime:null
          };
          http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
            let data =[];

            const checkVoluData=(data, target, sign)=>{
              let index = tool.serachIndexData(target, data, "bazaarName")
              let volu = data[index].turnoverDtos[0][sign]
              if(volu===0){
                volu =`&nbsp;`
              }
              return volu
            }

            res.map(item=>{
              const obj = {
                  bazaarName: item.vegetableName,
                  suSums: checkVoluData(item.vegetableTurnoverDtos, "合计", "sum"),
                  suSuburb: checkVoluData(item.vegetableTurnoverDtos, "合计", "jiaoCai"),
                  suExternal: checkVoluData(item.vegetableTurnoverDtos, "合计", "waiCai"),
                  bszSums: checkVoluData(item.vegetableTurnoverDtos, "白沙洲", "sum"),
                  bszSuburb: checkVoluData(item.vegetableTurnoverDtos, "白沙洲", "jiaoCai"),
                  bszExternal: checkVoluData(item.vegetableTurnoverDtos, "白沙洲", "waiCai"),
                  hjtSums: checkVoluData(item.vegetableTurnoverDtos, "皇经堂", "sum"),
                  hjtSuburb: checkVoluData(item.vegetableTurnoverDtos, "皇经堂", "jiaoCai"),
                  hjtExternal: checkVoluData(item.vegetableTurnoverDtos, "皇经堂", "waiCai"),
                  sjmSums: checkVoluData(item.vegetableTurnoverDtos, "四季美", "sum"),
                  sjmSuburb: checkVoluData(item.vegetableTurnoverDtos, "四季美", "jiaoCai"),
                  sjmExternal: checkVoluData(item.vegetableTurnoverDtos, "四季美", "waiCai"),
                  // qsSums: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // qsSuburb: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // qsExternal: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // qsmSums: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // qsmSuburb: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // qsmExternal: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // hpSums: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // hpSuburb: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // hpExternal: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // hjxSums: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // hjxSuburb: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId),
                  // hjxExternal: checkVoluData(item.vegetableTurnoverDtos, item.vegetableId)
              }
              data.push(obj)
            })
            self.renderTabeCon(table, data, tool[tabConf])
          }, (err) => {
            console.log(err)
          }, urlObj.token)
        },
        toolbarPrice($, form, table, bar, msn, initArget, urlObj, url, tabConf, arrPath, getList, opt){  //头工具栏事件
          const that = this;
          table.on(bar, (obj) => {
            const checkStatus = table.checkStatus(obj.config.id);
            switch(obj.event){
              case 'getCheckData':
                let getCheckData = checkStatus.data,
                  ids = [];
                if(getCheckData.length !== 0){
                  layui.each(getCheckData, (index, item)=>{
                    ids.push(item.pkId)
                  });
                  let Examines ={
                    state:7,
                    pkId:ids
                  };
                  that.checkInfoPrice($, Examines, arrPath[0]);
                  that[getList]($, table, urlObj, url, tabConf);
                } else {
                  layer.msg(msn, {icon: 5})
                }

                break;
              case 'getCheckLength':
                let getChecklist = checkStatus.data,
                  pids = [];
                if(getChecklist.length !==0 ){
                  layui.each(getChecklist, (index, item)=>{
                    pids.push(item.pkId)
                  });
                  let sends ={
                    state:5,
                    pkId:pids
                  };
                  that.checkInfoPrice($, sends, arrPath[0]);
                  that[getList]($, table, urlObj, url, tabConf);
                } else {
                  layer.msg(msn, {icon: 5})
                }
                break;
              case 'isRemove':
                let removeData = checkStatus.data,
                  arr = [];
                if(removeData.length !==0){
                  layui.each(removeData, (index, item)=>{
                    arr.push(item.pkId)
                  });
                  that.removePrice($, arr, arrPath[1]);
                  //that[getList]($, table, urlObj, url, tabConf);
                } else {
                  layer.msg(msn, {icon: 5})
                }

                break;
              case 'addPrice':
                switch (opt){
                  case "vegetable":
                    let veget = {
                      maketsPro: ""
                      ,variety: ""
                      ,jiaoCai: ""
                      ,waiCai: ""
                      ,plow: ""
                      ,pmax: ""
                    }
                    form.val(initArget, veget);
                    break;
                  case "grainOil":
                    let gol = {
                      maketsPro: ""
                      ,variety: ""
                      ,guiGe: ""
                      ,danWei: ""
                      ,price: ""
                    }
                    form.val(initArget, gol)
                    break;
                  case "aquatic":
                    let aqc = {
                      maketsPro: ""
                      ,variety: ""
                      ,guiGe: ""
                      ,priceMax: ""
                      ,priceLow: ""
                    }
                    form.val(initArget, aqc)
                    break;
                  case "liveStock":
                    let lsk = {
                      maketsPro: ""
                      ,variety: ""
                      ,price: ""
                    }
                    form.val(initArget, lsk)
                    break;
                }
                // form.val(initArget, {
                //   "maketsPro": ""
                //   ,"variety": ""
                //   ,"priceLowtitle": ""
                //   ,"priceMaxtitle": ""
                //   ,"waicaititle": ""
                //   ,"jiaocaititle": ""
                // });
                that.noticeShow($, form, '#std')
                break;
              case 'copyPrice':
                let copydata = checkStatus.data;
                if(copydata.length !==0){
                  let arr =[]
                  switch (opt){
                    case "vegetable":
                      layui.each(copydata, (index, item) => {
                        //console.log(item)
                        let veget = new vegetable(item.shiChangId-0, item.pinZhongId-0, item.jiaoCai-0, item.waiCai-0, item.plow-0, item.pmax-0, urlObj.adduser)
                        arr.push(veget)
                      })
                      break;
                    case "grainOil":
                      layui.each(copydata, (index, item) => {
                      let gol = new grainOil(item.shiChangId-0, item.pinZhongId-0, item.guiGe, item.danWei, item.price-0, urlObj.adduser)
                        arr.push(gol)
                      })
                      break;
                    case "aquatic":
                      layui.each(copydata, (index, item) => {
                      let aqc = new aquatic(item.shiChangId-0, item.pinZhongId-0, item.guiGe, item.priceMax-0, item.priceLow-0, urlObj.adduser)
                        arr.push(aqc)
                      })
                      break;
                    case "liveStock":
                      layui.each(copydata, (index, item) => {
                      let lsk = new liveStock(item.areaId-0, item.pinZhongId-0, item.price-0, urlObj.adduser)
                        arr.push(lsk)
                      })
                      break;
                  }
                  // layui.each(copydata, (index, item) => {
                  //   //console.log(item)
                  //   let target ={
                  //     shiChangId: item.shiChangId,
                  //     pinZhongId: item.pinZhongId,
                  //     plow: item.priceLow,
                  //     pmax: item.priceMax,
                  //     jiaoCai: item.jiaoCai,
                  //     waiCai: item.waiCai,
                  //     adduser:urlObj.adduser
                  //   }
                  //   arr.push(target)
                  // })
                  that.putAddInfo($, arr, arrPath[2])
                  that[getList]($, table, urlObj, url, tabConf);
                } else {
                  layer.msg(msn, {icon: 5})
                }
                break;
              case "setList":
                that[getList]($, table, urlObj, url, tabConf,)
                break
            }
          });
        },
        getStatistisData ($, table, obj, url, tabConf, that) {   //获取统计汇总
            const self = this||that;
            const params = {
                bazaarId: obj.bazaarId,
                endTime: obj.endTime,
                ids: obj.ids,
                startTime: obj.startTime
            };

            http.interactive($).commPost(baseConfig.serUrl+url, params, (res)=>{
              let data =[];
              $.each(res, (index, item ) => {
                const obj = {
                  name: item.name,
                  currentPeriod: item.currentPeriod,
                  priorPeriod: item.priorPeriod,
                  correspondPeriod: item.correspondPeriod,
                  chainIndex: item.chainIndex,
                  correspondIndex: item.correspondIndex
                };
                data.push(obj)
              });
              self.renderTabeCensus(table, data, tool[tabConf])
            }, (err) =>{
              console.log(err)
            }, urlObj.token)
        },
        submitTip ($, table, form, target, url, hanldFn, tabConf, opt) {   //监听提交
            const that = this;

            form.verify({
              staDate(value){
                console.log(value)
              }
            })

            form.on(`submit(${target})`, (data) => {
              let arr =[],
                bazzs = [];
              if (data.field.staDate||data.field.endDate){
                let sta = tool.transTime(data.field.staDate)
                let end = tool.transTime(data.field.endDate)
                if(end-sta<0){
                  layFrame.showMsg("时间先后有问题，请重填", 0, 3000)
                  return
                }
              } else {
                layFrame.showMsg("时间不能为空", 0, 3000)
                return
              }
              $.each(data.field, (key, item)=>{
                  if(item==="on"){
                    if(key!=="close"){
                      arr.push(key)
                    }
                  }
              })

              bazzs.push(data.field.interest);
              //console.log(data.field)

              if(!arr.length){
                layFrame.showMsg("请选择您要查询的品种", 0, 3000);
                return
              } else {
                switch (opt){
                  case "vegetable":
                    let veget = new queryObj(bazzs, data.field.endDate, arr, urlObj.pageCurrent, urlObj.pageSize, data.field.staDate, data.field.interest)
                    hanldFn($, table, veget, url, tabConf, that)
                    break;
                  case "grainOil":
                    let gol = new queryObj(bazzs, data.field.endDate, arr, urlObj.pageCurrent, urlObj.pageSize, data.field.staDate, data.field.interest)
                    hanldFn($, table, gol, url, tabConf, that)
                    break;
                  case "aquatic":
                    let aqc = new queryObj(bazzs, data.field.endDate, arr, urlObj.pageCurrent, urlObj.pageSize, data.field.staDate, data.field.interest)
                    hanldFn($, table, aqc, url, tabConf, that)
                    break;
                  case "liveStock":
                    let lsk = new queryObj(bazzs, data.field.endDate, arr, urlObj.pageCurrent, urlObj.pageSize, data.field.staDate, data.field.interest)
                    hanldFn($, table, lsk, url, tabConf, that)
                    break;
                }
              }



              // const obj ={
              //   bazaarId: bazzs,
              //   endTime: data.field.endDate,
              //   ids: arr,
              //   pageCurrent: urlObj.pageCurrent,
              //   pageSize: urlObj.pageSize,
              //   startTime: data.field.staDate
              // }
              //
              // hanldFn($, table, obj, url, tabConf, that)

              return false;
            });
        },
        renderTabeCon (table, data, tabConf) {
            table.render(tabConf(data));
        },
        renderTabeCensus (table, data, tabSta) {
            table.render(tabSta(data));
        },
        noticeShow ($, form, target) {   //触发弹框事件
            this.indexOpn = layer.open({
                type: 1
                ,title:"新增信息"
                , content: $(target)
                , success: function (layero) {
                    form.render()
                }
                ,yes: function(index, layero){
                  layer.close(index);
                  //console.log(123)
                }
            });
        },
        handeTool ($, form, table, target, mge, initArget, url, tabConf, delPath, getList, opt) {   //监听行工具事件
          const that = this;
          table.on(target, (obj) => {
              const data = obj.data;
              if(obj.event === 'del'){
                layer.confirm(mge, function(index){
                  that.removePrice($, data.pkId, delPath)
                  obj.del(data);
                  layer.close(index);
                  //that[getList]($, table, urlObj, url, tabConf)
                });
              } else if(obj.event === 'edit'){
                // layer.prompt({
                //   formType: 2
                //   ,value: data.jiaoCai,
                //   value: data.waicai
                // }, function(value, index){
                //   obj.update({
                //     jiaoCai: value,
                //     waicai: value
                //   });
                //   layer.close(index);
                // });
                console.log(data)
                switch (opt){
                  case "vegetable":
                    //let veget = new vegetable(data.shiChangId, data.pinZhongId, data.jiaoCai, data.waiCai, data.priceLow, data.priceLow, urlObj.adduser)
                    let veget = {
                        maketsPro: data.shiChangId
                        ,variety: data.pinZhongId
                        ,plow: data.priceLow
                        ,pmax: data.priceMax
                        ,jiaoCai: data.jiaoCai
                        ,waiCai: data.waiCai
                        ,pkId: data.pkId
                    }
                    form.val(initArget, veget)
                    break;
                  case "grainOil":
                    //let gol = new grainOil(data.shiChangId, data.pinZhongId, data.guiGe, data.danWei, data.price, urlObj.adduser)
                    let gol = {
                      maketsPro: data.shiChangId
                      ,variety: data.pinZhongId
                      ,guiGe: data.guiGe
                      ,danWei: data.danWei
                      ,price: data.price
                      ,pkId: data.pkId
                    }
                    form.val(initArget, gol)
                    break;
                  case "aquatic":
                    //let aqc = new aquatic(data.shiChangId, data.pinZhongId, data.guiGe, data.priceMax, data.priceLow, urlObj.adduser)
                    let aqc = {
                      maketsPro: data.shiChangId
                      ,variety: data.pinZhongId
                      ,guiGe: data.guiGe
                      ,priceMax: data.priceMax
                      ,priceLow: data.priceLow
                      ,pkId: data.pkId
                    }
                    form.val(initArget, aqc)
                    break;
                  case "liveStock":
                    //let lsk = new liveStock(data.shiChangId, data.pinZhongId, data.price, urlObj.adduser)
                    let lsk = {
                      maketsPro: data.areaId
                      ,variety: data.pinZhongId
                      ,price: data.price
                      ,pkId: data.pkId
                    }
                    form.val(initArget, lsk)
                    break;
                }

                // form.val(initArget, {
                //   maketsPro: data.shiChangId
                //   ,variety: data.pinZhongId
                //   ,priceLowtitle: data.priceLow
                //   ,priceMaxtitle: data.priceMax
                //   ,waicaititle: data.jiaoCai
                //   ,jiaocaititle: data.waiCai
                // })
                this.noticeShow($, form, '#etd')

              }
          });
        },
        checkInfoPrice ($, obj, url) {   //审核提交,批量退回
            let params
            if(tool.isArrayFn(obj.pkId)){
            params = obj.pkId
            } else{
            params = []
            params.push(obj.pkId)
            }
            http.interactive($).commPost(baseConfig.serUrl+`${url}?state=${obj.state}`, params, (res)=>{
            console.log(res)
            }, (err)=>{
            console.log(err)
            }, urlObj.token)
        },
        removePrice ($, obj, url) {  //删除信息操作
            let params
            if(tool.isArrayFn(obj)){
              params = obj
            } else{
              params = []
              params.push(obj)
            }
            console.log(params)
            http.interactive($).commPost(baseConfig.serUrl+url, params, (res)=>{
              console.log(res)
            }, (err)=>{
              console.log(err)
            }, urlObj.token)
        },
        putAddInfo ($, obj, url) {    //新增一条
          let params
          if(tool.isArrayFn(obj)){
            params = obj
          } else{
            params = []
            params.push(obj)
          }
          //console.log(params)
          http.interactive($).commPut(baseConfig.serUrl+url, params, (res)=>{
            console.log(res)
          }, (err)=>{
            console.log(err)
          }, urlObj.token)
        },
        putEditInfo ($, obj, url) {    //编辑当前一条信息
          let params = obj
          http.interactive($).commPost(baseConfig.serUrl+url, params, (res)=>{
            console.log(params)
            console.log(res)
          }, (err)=>{
            console.log(err)
          }, urlObj.token)
        },
        submitAddPrice ($, table, form, target, hanldFn, url, tabConf, path, listFn, opt) {  //表单btn提交新增信息
            form.on(`submit(${target})`, (data) => {
              switch (opt){
                case "vegetable":
                  let veget = new vegetable(data.field.maketsPro-0, data.field.variety-0, data.field.jiaoCai-0, data.field.waiCai-0, data.field.pmax-0, data.field.plow-0, urlObj.adduser, data.field.pkId-0)
                  hanldFn($, veget, url)
                  break;
                case "grainOil":
                  let gol = new grainOil(data.field.maketsPro-0, data.field.variety-0, data.field.guiGe, data.field.danWei, data.field.price-0, urlObj.adduser, data.field.pkId-0)
                  hanldFn($, gol, url)
                  break;
                case "aquatic":
                  let aqc = new aquatic(data.field.maketsPro-0, data.field.variety-0, data.field.guiGe, data.field.priceMax-0, data.field.priceLow-0, urlObj.adduser, data.field.pkId-0)
                  hanldFn($, aqc, url)
                  break;
                case "liveStock":
                  let lsk = new liveStock(data.field.maketsPro-0, data.field.variety-0, data.field.price-0, urlObj.adduser, data.field.pkId-0, data.field.pkId-0)
                  hanldFn($, lsk, url)
                  break;
              }

              if(target ==="demo4"||"demo5"){
                layer.close(this.indexOpn);
              }
              this[listFn]($, table, urlObj, path, tabConf)
              return false;
            });
        },
        checkSwitch($, form, laydate, checkFun, swhFun, btn, target, type, attr, tpl, getVariety, searchId, pathArr, container, titles, date){
          const that = this
          //有一个未选中 取消全选
          form.on(checkFun, function(data){
            let item = $(target);
            for(let i = 0; i < item.length; i++) {
              if(item[i].checked === false) {
                $(btn).prop(attr, false);
                form.render(type);
                break;
              }
            }
            //如果都勾选了  勾上全选
            let all = item.length;
            for(let i = 0; i < all; i++) {
              if(item[i].checked === true) {
                all--;
              }
            }
            if(all === 0) {
              $(btn).prop(attr, true);
              form.render(type);
            }
          });


          //反选
          form.on(swhFun, function(data){
            if(this.checked) {
              let a = data.elem.checked;
              if(a === true) {
                $(target).prop(attr, true);
                form.render(type);
              } else {
                $(target).prop(attr, false);
                form.render(type);
              }
            } else {
              let item = $(target);
              item.each(function() {
                if($(this).prop(attr)) {
                  $(this).prop(attr, false);
                } else {
                  $(this).prop(attr, true);
                }
              })
              that[getVariety]($, form, laydate, pathArr, searchId, container, titles, tpl, date)
              form.render(type);
            }

          });
        }
    }
})
