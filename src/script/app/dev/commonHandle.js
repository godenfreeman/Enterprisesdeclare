define([
  "./layer",
  "./http-lay",
  "conf/baseConfig",
  './tool',
  './table',
  './declare',
  './commpany',
  "conf/urlObj"
],function (layFrame, http, baseConfig, tool, tab, Declare, Compy, urlObj) {
    return {
      countdown:5,
      setUserName($, target){
        $(target).html(`${urlObj.userName}`)
      },
      getVariety ($, form, url, container, tpl, renType) {   //从后端获取查询条件
        http.interactive($).commPost(baseConfig.serUrl+url, {},(res) => {
          let obj = {
            areas:res.data.listArea,
            types:res.data.listType
          }
          layFrame.setlayTpl("laytpl", container, tpl, obj)
          setTimeout(()=>form.render(renType), 50)
        }, (err) => {
          console.log(err)
        }, "");
      },
      getDateList ($, table, url, tabConf, that) {
        const self = this||that;
        const params = {
          enterpriseId:urlObj.enterpriseId
        };
        // http.interactive($).commPost(url, params, (res) => {
        http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
          console.log(res.data.enterpriseInfo)
          const baseInfo = res.data.enterpriseInfo
          let data =[];
          $.each(baseInfo.enterpriseDeclareList, (index, item ) => {
            let obj = new Declare(
              baseInfo.id,
              item.enterpriseId,
              baseInfo.enterpriseName,
              tool.handleType(baseInfo.enterpriseType),
              baseInfo.linkman,
              item.createTime,
              tool.handleStatus(item.enterpriseDeclareReplyList[0].declareState),
            )
            data.push(obj)
          });
          console.log(data)
          self.renderTabeCon(table, data, tab[tabConf])
        }, (err) => {
          console.log(err)
        }, "")
      },
      renderTabeCon (table, data, tabConf) {
        table.render(tabConf(data));
      },
      getEnterprise ($, url, tpl, target, that) {
        const self = this||that;
        const params = {
          enterpriseId:urlObj.enterpriseId
        };
        http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
          // http.interactive($).commPost(url, params, (res) => {
          console.log(res)
          const val = res.data.enterpriseInfo
          let obj = {
            comName:val.enterpriseName,
            // val.legal,
            // val.idCard,
            contract:val.linkman,
            tel:val.phone,
            area:val.enterpriseAreaInfo.areaName,
            // val.addr,
            comType:val.enterpriseTypeInfo.typeName,
            enterpriseScale:tool.strToArr(baseConfig.serUrl, val.enterpriseScale),
            enterpriseBenefit:tool.strToArr(baseConfig.serUrl, val.enterpriseBenefit),
            enterpriseHonesty:tool.strToArr(baseConfig.serUrl, val.enterpriseHonesty),
            baseScale:tool.strToArr(baseConfig.serUrl, val.baseScale),
            bandScale:tool.strToArr(baseConfig.serUrl, val.bandScale)
          }


          layFrame.setlayTpl("laytpl", target, tpl, obj)
        }, (err) => {
          console.log(err)
        }, "")
      },
      clickVerifyCode($, target, url, tel){
        let that = this,timeTask;
        $(target).click(()=>{
          let tel =$("#phoneNum").val();
          let reg = /^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/;
          if(reg.test(tel)){
            timeTask = setInterval(function () {
              if (that.countdown > 0) {
                $(target).attr("disabled","disabled")
                $(target).addClass("layui-btn-disabled");
                $(target).text(that.countdown + "秒后点此重新发送");
                that.countdown--;
              } else {
                $(target).removeAttr("disabled")
                $(target).removeClass("layui-btn-disabled");
                $(target).text("点此重新发送");
                that.countdown=5;
                clearInterval(timeTask)
              }
            }, 1000)

            console.log(tel)
            // http.interactive($).commPost(baseConfig.serUrl+url, {tel},(res) => {
            //   if(res){
            //     console.log(res);
            //   }
            // }, (err) => {
            //   console.log(err)
            // }, "");
          } else {
            layFrame.showMsg("您输入的手机号码有误，请重新输入", 0, 3000);
          }

        })
      },
      checkVerifyCode($, url, obj){
        const dtd = $.Deferred();
        http.interactive($).commPost(baseConfig.serUrl+url, obj,(res) => {
          dtd.resolve(res);
        }, (err) => {
          dtd.reject(err);
        }, "");
        return dtd.promise();
      },
      uploadfile($, upload, elem, url, bindAction, target){
        const   dtd = $.Deferred(),
                that = this;
        let arrImg =[];

        //多文件列表示例
        const demoListView = $(target)  //'#demoList'
          ,uploadListIns = upload.render({
          elem: elem //'#testList'
          ,url: baseConfig.serUrl+url
          ,accept: 'file'
          ,multiple: true
          ,auto: false
          ,bindAction: bindAction //'#testListAction'
          ,choose: function(obj){
            let files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列

            //读取本地文件
            obj.preview(function(index, file, result){

              let fileType = file.name.split(".")[file.name.split(".").length-1]

              obj.resetFile(index, file, `${new Date().getTime()}.${fileType}`)

              let tr = $(['<tr id="upload-'+ index +'">'
                ,'<td>'+ file.name +'</td>'
                ,'<td>'+ (file.size/1014).toFixed(1) +'kb</td>'
                ,'<td>等待上传</td>'
                ,'<td>'
                ,'<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>'
                ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                ,'</td>'
                ,'</tr>'].join(''));

              //单个重传
              tr.find('.demo-reload').on('click', function(){
                obj.upload(index, file);
              });

              //删除
              tr.find('.demo-delete').on('click', function(){
                delete files[index]; //删除对应的文件
                tr.remove();
                uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
              });

              demoListView.append(tr);
            });
          }
          ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
            //console.log(obj)
          }
          ,done: function(res, index, upload){
            console.log(res)
            if(res.code === 0){ //上传成功
              res.data.map((item, index)=>{
                arrImg.push(item)
              })
              let tr = demoListView.find('tr#upload-'+ index)
                ,tds = tr.children();
              tds.eq(2).html('<span style="color: #5FB878;">上传成功</span>');
              tds.eq(3).html(''); //清空操作
              return delete this.files[index]; //删除文件队列已经上传成功的文件
            }
            this.error(index, upload);
          }
          ,allDone: function(res){ //当文件全部被提交后，才触发
            dtd.resolve(arrImg)
          }
          ,error: function(index, upload){
            let tr = demoListView.find('tr#upload-'+ index)
              ,tds = tr.children();
            tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
            tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
            dtd.reject(upload)
          }
        });


        return dtd.promise();


      },
      submitRegister($, target, params, url){
        if(target){
          $(target).click(()=>{

            console.log(params)
            if(params){
              console.log(params)
            }

            http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
              console.log(res)
              if(res.code===1){
                layFrame.showMsg(`注册成功`, 1, 3000)
                setTimeout(()=>{
                  window.open("login.html", "_self")
                },3000)

              } else{
                layFrame.showMsg(`注册失败，请重新上传资料后再提交注册`, 0, 2000)
              }
            }, (err) => {
              console.log(err)
            }, "")
          })
        } else {
          http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
            console.log(res)
            if(res.code===1){
              layFrame.showMsg(`你好，${urlObj.userName}修改密码成功`, 1, 2000)
              sessionStorage.clear()
              setTimeout(()=>{
                window.open("login.html", "_self")
              },3000)

            } else{
              layFrame.showMsg(`注册失败，请重新上传资料后再提交注册`, 0, 2000)
            }
          }, (err) => {
            console.log(err)
          }, "")
        }

      },
      checkinputVal(form){
        //自定义验证规则
        form.verify({
          username: (value)=>{
            if(value.length < 5){
              return '用户名不能少于5个字符啊';
            }
          }
          ,pass:[
            /^[\S]{6,12}$/
            ,'密码必须6到12位，且不能出现空格'
          ]
          ,idCode:[
            /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/
            ,'请输入正确的18位身份证号'
          ]
          ,phone:[
            /^(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/
            ,"请输入正确的联系人电话号码"
          ]
          ,fullname:[
            /^([\u4e00-\u9fa5]{1,20}|[a-zA-Z\.\s]{1,20})$/
            ,"请输入正确的企业名称"
          ]
        });
      },
      exitHandle($, target, keys, page){
        $(document).on('click', `#${target}`, () => {
          $.each(keys, (index, item) => {
            http.storage().removeItem(item)
          })
          window.open(page, "_self")
        })
      },
      toolbarPrice($, form, table, bar, url){  //头工具栏事件
        const that = this;
        table.on(bar, (obj) => {
          const checkStatus = table.checkStatus(obj.config.id);
          switch(obj.event){
            case 'addDeclare':
              window.open(`${url}`, "_self")
              break;
            case "setList":
              alert(465)
              break
          }
        });
      },
      submitUpFile($, target, params, url){
          $(target).click(()=>{
            console.log(params)
            if(params){
              console.log(params)
            }
            http.interactive($).commPost(baseConfig.serUrl+url, params, (res) => {
              console.log(res)
              if(res.code===1){
                layFrame.showMsg(`申请成功`, 1, 3000)
                setTimeout(()=>{
                  window.open("enterList.html", "_self")
                },3000)

              } else{
                layFrame.showMsg(`申请失败，请重新上传资料后再提交注册`, 0, 2000)
              }
            }, (err) => {
              console.log(err)
            }, "")
          })
      },
      tickShowHid($, target, hidel, showt){
        $(target).click(()=>{
          $(hidel).hide();
          $(showt).show()
        })
      }
    }
});
