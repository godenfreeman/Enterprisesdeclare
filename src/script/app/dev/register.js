define([],function () {
  return class regMod {
    constructor(enterpriseName, phone, linkman, enterpriseArea, enterpriseType){
      this.enterpriseName = enterpriseName;
      this.phone = phone;
      this.linkman = linkman;
      this.enterpriseArea = enterpriseArea;
      this.enterpriseType = enterpriseType;
    }
  }
})
