define(["mock"],function (Mock) {
  return {
    setMock(Class){
      const Random = Mock.Random // Mock.Random 是一个工具类，用于生成各种随机数据
      let data = [] // 用于接受生成数据的数组
      for(let i = 0; i < 100; i ++) { // 可自定义生成的个数
        let template = new Class(
          Random.integer(1001, 1010),
          Random.integer(2001, 2010),
          Random.ctitle(),
          Random.integer(1, 3),
          Random.cname(),
          Random.date("yyyy-MM-dd"),
          Random.integer(4,6),
        )
        data.push(template)
      }
      Mock.mock('/data/index', 'post', data) // 根据数据模板生成模拟数据
    },
    setEnter(Class){
      const Random = Mock.Random // Mock.Random 是一个工具类，用于生成各种随机数据
      let template = new Class(
        Random.ctitle(),
        Random.cname(),
        Random.id(),
        Random.cname(),
        Random.integer(13730939485, 13730939785),
        Random.region(),
        Random.city(),
        Random.cword('农业加工流通种植', 4)
      );
      Mock.mock('/data/info', 'post', template) // 根据数据模板生成模拟数据

    }
  }
});
