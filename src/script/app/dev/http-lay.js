define([],function () {
    return {
        interactive($){
            const commPost = (url,data,callback,ecallback,accessToken) =>{
                $.ajax({
                    headers:{token: accessToken},
                    type: 'POST',
                    url:url,
                    data:JSON.stringify(data),
                    dataType:"json",
                    contentType:"application/json",
                    cache: false,
                    success:function(data){
                        if(callback){

                            callback(data);
                        }
                    },
                    error:ecallback||myerror
                });

            };
            const commGet = (url,data,callback,ecallback,accessToken) =>{
                $.ajax({
                    headers:{token: accessToken},
                    type: 'GET',
                    url:url,
                    data:data,
                    dataType:"json",
                    contentType:"application/json",
                    cache: false,
                    success:function(data){
                        if(callback){
                            callback(data);
                        }
                    },
                    error:ecallback||myerror
                });

            };
            const commDel = (url,data,callback,ecallback,accessToken)=>{
                $.ajax({
                    headers:{token: accessToken},
                    type: 'DELETE',
                    url:url,
                    data:data,
                    dataType:"json",
                    contentType:"application/json",
                    cache: false,
                    success:function(data){

                        if(callback){


                            callback(data);
                        }

                    },
                    error:ecallback||myerror
                });

            };
            const commPut = (url,data,callback,ecallback,accessToken) => {
                $.ajax({
                    headers:{token: accessToken},
                    type: 'PUT',
                    url:url,
                    data:JSON.stringify(data),
                    dataType:"json",
                    contentType:"application/json",
                    cache: false,
                    success:function(data){

                        if(callback){

                            callback(data);
                        }
                    },
                    error:ecallback||myerror
                });

            };
           const myerror = (XMLHttpRequest, textStatus, errorThrown) =>{
                if( olUtile!=null){
                  olUtile.closeLoading();
                }
                //alert(textStatus+""+errorThrown);
                //  alert(XMLHttpRequest.readyState);
           };
           const getQueryString = (name)=>{   //获取地址栏参数
                let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                let r = window.location.search.substr(1).match(reg);
                if(r != null)
                  return unescape(r[2]);
                return null;
           }
           return {commPost, commGet, commDel, commPut, getQueryString}
        },
        storage(){
          const getItem = (key) => { //假如浏览器支持本地存储则从sessionStorage里getItem，否则乖乖用Cookie
            return window.sessionStorage ? sessionStorage.getItem(key) : Cookie.read(key);
          };
          const setItem = (key, val) => { //假如浏览器支持本地存储则调用sessionStorage，否则乖乖用Cookie
            if(window.sessionStorage) {
              sessionStorage.setItem(key, val);
            } else {
              Cookie.write(key, val);
            }
          };
          const  removeItem = (key) => {
            if(window.sessionStorage) {
              sessionStorage.removeItem(key);
            } else {
              Cookie.write(key, '', -1);
            }
          }
          return {getItem, setItem, removeItem}
        }
    }
})
