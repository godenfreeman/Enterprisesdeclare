define(["./http-lay"],function (http) {
    return {
        tabConf(data){
          const tabCf = {
            elem: '#test'
            ,cellMinWidth: 80
            ,toolbar: '#toolbarDemo'
            ,title: '用户数据表'
            ,skin:'line'
            ,cols: [[
              ,{field:'oderId', title:'oderId',fixed: 'left', unresize: true, sort: true, hide:true}
              ,{field:'comId', title:'comId',fixed: 'left', unresize: true, hide:true}
              ,{field:'comName', title:'公司名称', align:'center'}
              ,{field:'comType', title:'公司类型', align:'center'}
              ,{field:'linkPrson', title:'录入人', align:'center'}
              ,{field:'declTime', title:'申报时间', align:'center'}
              ,{field:'Adstatus', title:'审核状态', align:'center'}
              ,{fixed: 'right', title:'操作', toolbar: '#barDemo', align:'center'}
            ]]
            ,data: data
            ,page: true
          };
          return tabCf
        }
    }
})

