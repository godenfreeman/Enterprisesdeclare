define(['crypto-js'],function (CryptoJS) {
  return {
    getAesString(data, key, iv){//加密
        let keys  = CryptoJS.enc.Utf8.parse(key);
        let ivs   = CryptoJS.enc.Utf8.parse(iv);
        let encrypted =CryptoJS.AES.encrypt(data, keys,
          {
            iv:ivs,
            mode:CryptoJS.mode.CBC,
            padding:CryptoJS.pad.Pkcs7
          });
        return encrypted.toString();    //返回的是base64格式的密文
    },
    getDAesString(encrypted, key, iv){//解密
        let keys  = CryptoJS.enc.Utf8.parse(key);
        let ivs   = CryptoJS.enc.Utf8.parse(iv);
        let decrypted =CryptoJS.AES.decrypt(encrypted, keys,
          {
            iv:ivs,
            mode:CryptoJS.mode.CBC,
            padding:CryptoJS.pad.Pkcs7
          });
        return decrypted.toString(CryptoJS.enc.Utf8);
    }
  }
});
