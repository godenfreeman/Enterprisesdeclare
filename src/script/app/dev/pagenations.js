define([
  "jquery",
  "pagination",
  "../dev/mouseMoving",
  "text!tpl/user.hbs",
  "text!tpl/student.hbs",
  "../dev/tpl"
],function ($, pagination, mou, user, student, tpl) {
  return {
    pageFn(url, pager, target){

      $.getJSON(url, function (data) {
        $(pager).pagination({
          dataSource: data.list,
          pageSize: 9,
          showGoInput: true,
          showGoButton: true,
          callback: function(data, pagination) {
            tpl.CreatTpl(target, user, data);
            $('.grid-content').mouseMove('.grid-shade')
          }
        })
      })

    },
    pageRender(data, pager, target){

          $(pager).pagination({
            dataSource: data.list,
            pageSize: 9,
            showGoInput: true,
            showGoButton: true,
            callback: function(data, pagination) {
              tpl.CreatTpl(target, student, data);
              $('.grid-content').mouseMove('.grid-shade')
            }
          })
  
      }
  }
})
