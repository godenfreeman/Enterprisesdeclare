define(["jquery"],function ($) {
    return {
        trim(str) {
            return $.trim(str)
        },
        findIndexData(target, data, param){
          let index = -1;
          $.each(data, (key, item) => {
            if(item[param] == target){
              index = key
            }
          });
          return index
        },
        getSessionHandle(key) {
          let sesVal;
          if(sessionStorage.getItem(key)){
            sesVal = sessionStorage.getItem(key);
            sessionStorage.removeItem(key)
          }
          return sesVal
        },
        trimTool(str){
          return str.replace(/[ ]/g,"");  //去除字符算中的空格
        },
        getUser() {
          var def = $.Deferred()
            return def
        }
    }
})
