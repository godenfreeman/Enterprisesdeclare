define(["jquery","text!tpl/nav.hbs","../dev/layer","../conf/navConf"], function ($, nav, layer, conf) {
    return {
        showNav() {

            //this.setNav();

            var nav = $('nav'),
                menue = $('nav h1'),
                main = $('main'),
                open = false,
                hover = false;

            menue.on('click', function () {
                open = !open ? true : false;
                nav.toggleClass('menu-active');
                main.toggleClass('menu-active');
                nav.removeClass('menu-hover');
                main.removeClass('menu-hover');
                console.log(open);
            });
            menue.hover(
                function () {
                    if (!open) {
                        nav.addClass('menu-hover');
                        main.addClass('menu-hover');
                    }
                },
                function () {
                    nav.removeClass('menu-hover');
                    main.removeClass('menu-hover');
                }
            );
        },
        setNav(){
          layer.setlayTpl('laytpl', 'menubar', nav, conf)
        }

    }
});
