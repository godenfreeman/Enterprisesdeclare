define(["jquery",  'handlebars'],function ($, Handlebars) {
    return {
      CreatTpl(target, tpl, data){
        const tplFn = Handlebars.compile(tpl);
        const theCompiledHtml = tplFn(data);
        $(target).html(theCompiledHtml);
      }
    }
})
