define(["./http-lay", "./utils"],function (http, utils) {
    return {
        saveStorage(key){
            let val;
            if (http.interactive("").getQueryString(key)!==null){
              if(key==="token"){
                val = http.interactive("").getQueryString(key)
                http.storage().setItem(key, val)
              } else {
                val = http.interactive("").getQueryString(key)
                let tooVal = this.setAES(val)
                http.storage().setItem(key, tooVal)
              }
              window.location.href=window.location.origin+window.location.pathname
            } else {
              if(key==="token"){
                val = http.storage().getItem(key)
              } else{
                if(key==="bazaarIds"){
                  val = this.getDAes(http.storage().getItem(key)).split(",");
                } else {
                  val = this.getDAes(http.storage().getItem(key))
                }
              }
            }
            return val
        },
        isArrayFn(value){
            if (typeof Array.isArray === "function") {
              return Array.isArray(value);
            }else{
              return Object.prototype.toString.call(value) === "[object Array]";
            }
        },
        exitHandle($, target, keys, page){
          $(document).on('click', `#${target}`, () => {
            $.each(keys, (index, item) => {
              http.storage().removeItem(item)
            })
            window.open(page, "_self")
          })
        },
        transTime(time){
          let dc = time.substring(0,19).replace(/-/g,'/');
          let timestamp = new Date(dc).getTime();
          return timestamp
        },
        setAES(data){ //加密
          const key  = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';  //密钥
          const iv   = '1234567812345678';
          let encrypted = utils.getAesString(data, key, iv); //密文
          //let encrypted1 = CryptoJS.enc.Utf8.parse(encrypted);
          return encrypted;
        },
        getDAes(data){//解密
          const keys  = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';  //密钥
          const ivs   = '1234567812345678';
          let decryptedStr = utils.getDAesString(data, keys,ivs);
          return decryptedStr;
        },
        handleStatus(val){ //判断状态
          let sta;
          switch (val){
            case 0:sta="申请中";
              break;
            case 1:sta="区审核";
              break;
            case 2:sta="区退回";
              break;
            case 3:sta="市审核";
              break;
            case 4:sta="市退回";
              break;
            case 5:sta="审核结束";
              break;
            default:sta="申请中"
              break;
          }
          return sta
        },
        handleType(val){ //判断类型
          let sta;
          switch (val){
            case 1:sta="农产品加工";
              break;
            case 2:sta="农产品流通";
              break;
            case 3:sta="农产品种植";
              break;
            default:sta="已审核"
          }
          return sta
        },
        serachIndexData(target, data, param){
          let index = -1;
          data.map((item, key) => {
            if(item[param] == target){
              index = key
              return
            }
          });
          return index
        },
        ergodicData(data){
          let index
          data.map((item) =>{
              index = item
          })
          return index
        },
        strToArr(url, data){
          let arr =[]
          const target = data.split(",")
          target.map((item, index)=>{
            console.log(url+item)
            arr.push(url+item)
          })
          return arr
        }
    }
})

