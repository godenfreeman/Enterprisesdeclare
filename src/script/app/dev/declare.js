define([],function () {
  return class Declare {
    constructor(oderId, comId, comName, comType, linkPrson,  declTime, Adstatus){
      this.oderId = oderId;
      this.comId = comId;
      this.comName = comName;
      this.comType =comType;
      this.linkPrson = linkPrson;
      this.declTime = declTime;
      this.Adstatus = Adstatus;
    }
  }
})
