define([],function () {
  return class queryObj {
    constructor(bazaarId, endTime, ids, pageCurrent, pageSize, startTime, areaId){
      this.bazaarId = bazaarId;
      this.endTime =endTime;
      this.ids = ids;
      this.pageCurrent = pageCurrent;
      this.pageSize = pageSize;
      this.startTime = startTime;
      this.areaId = areaId;
    }
  }
})
