define(["dev/tool"],function (tool) {
    return {
        id:tool.saveStorage("id"),
        userName:tool.saveStorage("userName"),
        enterpriseId: tool.saveStorage("enterpriseId")
    }
})
