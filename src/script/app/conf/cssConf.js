define(
  {
    cdnjs:`https://cdnjs.cloudflare.com/ajax/libs/`,
    csslist: [
      './js/vendor/layui/src/css/layui.css',
      `${cdnjs}font-awesome/4.7.0/css/font-awesome.min.css`,
      //'https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css',
      './css/style.css',
      './css/index.css',
      `${cdnjs}animate.css/3.7.0/animate.min.css`
      //'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css'
    ]
  }
);
